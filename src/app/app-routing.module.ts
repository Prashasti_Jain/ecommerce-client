import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginSuccessComponent } from './login-success/login-success.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotpwdComponent } from './forgotpwd/forgotpwd.component'; 
import { UserprofileComponent } from './userprofile/userprofile.component';
import { OTPComponent } from './otp/otp.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { HomepageComponent } from './homepage/homepage.component';
import { AdminApprovalComponent } from './admin-approval/admin-approval.component';
import { ProductdetailsComponent } from './productdetails/productdetails.component';
import { RatePrroductComponent } from './rate-prroduct/rate-prroduct.component';
import { PlaceorderComponent } from './placeorder/placeorder.component';
import { CartComponent } from './cart/cart.component';
import { AvgRatingComponent } from './avg-rating/avg-rating.component';
import { MsgDialogueComponent } from './msg-dialogue/msg-dialogue.component';
import { UserprofileAdminComponent } from './userprofile-admin/userprofile-admin.component';
import { UserprofileVendorComponent } from './userprofile-vendor/userprofile-vendor.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { FilterProductComponent } from './filter-product/filter-product.component';
import { WishlistComponent } from './wishlist/wishlist.component';



const routes: Routes = [
  {path :'',component : HomepageComponent},
  {path :'loginSuccess',component :LoginSuccessComponent},
  {path :'register',component:RegisterComponent},
  {path :'login',component:LoginComponent},
  {path :'forgotpwd',component:ForgotpwdComponent},
  {path :'userprofile',component:UserprofileComponent},
  {path :'userprofile/:submenu',component:UserprofileComponent},
  {path :'otp',component:OTPComponent},
  {path :'addProduct', component:AddproductComponent },
  {path :'addProduct/:submenu', component:AddproductComponent },
  {path :'homepage/:prodName',component: HomepageComponent},
  {path :'homepage',component: HomepageComponent},
  {path :'admin', component: AdminApprovalComponent},
  {path :'product/:prodId', component: ProductdetailsComponent},
  // {path :'rating', component: RatePrroductComponent},
  {path :'placeorder', component:PlaceorderComponent},
  {path :'cart', component:CartComponent},
  // {path :'rate', component: AvgRatingComponent},
  // {path :'msg', component: MsgDialogueComponent},
  // {path :'profileAdmin', component: UserprofileAdminComponent},
  // {path :'profileVendor', component: UserprofileVendorComponent},
  {path :'orderdetails', component: OrderDetailsComponent},
  // {path :'nav', component:NavbarComponent},
  // {path :'foot', component:FooterComponent},
  // {path :'filt', component:FilterProductComponent},
  {path :'wishlist', component:WishlistComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
