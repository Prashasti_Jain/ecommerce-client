import { Component, OnInit, ViewChild, ɵisBoundToModule__POST_R3__ } from '@angular/core';
import { User } from '../user';
import {RegistrationService } from '../registration.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { MsgDialogueComponent } from '../msg-dialogue/msg-dialogue.component';
import { AddproductService } from '../shared/addproduct.service';
import { WishlistComponent } from '../wishlist/wishlist.component';
import { MyratingsComponent } from '../myratings/myratings.component';
import { Address } from '../address';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {
  @ViewChild(MsgDialogueComponent) msgDiag:MsgDialogueComponent;
  @ViewChild(WishlistComponent) wishlistCmp:WishlistComponent;
  @ViewChild(MyratingsComponent) myReviewsCmp:MyratingsComponent;
  hideSubMenu=true;
  hidewishlist=true;
  hideMyRating=true;
  user:User;
  isShow=false;
  isAddressDetailsShow=true;
  msg = '';
  varfname:string;
  notEditUser=true;
  notEditAddress=true;
  userRole;
  hideMenu=true;
  userCopy;
  allAddress:Address[]=[];
  extraOptionMenu:boolean[]=[];
  // newAddress:Address=new Address();
  edit1Address:Address = new Address();
  hideEditingAddress=true;

  showSubmenu(){
    this.hideSubMenu=!this.hideSubMenu;
  }
  editUserDetails(){
    // console.log("copy of user",this.userCopy)
    this.notEditUser=false;

  }
  editAddressDetails(){
    this.notEditAddress=false;
  }

  cancelEdit(){
    // console.log("copy of user 2",this.userCopy)
    this.user= this.userChange(this.userCopy,this.user);
    this.notEditUser=true;
    this.notEditAddress=true;
    this.hideEditingAddress=true;
    this.hideAllExtraOption();
  }
  noteditUserDetails(){
    this.notEditUser=true;
    this._service.updateUserDetails(this.user).subscribe(
      data=>{
        console.log("User Details updated")
        this.userCopy=this.userChange(this.user,this.userCopy);
      },
      error=>console.log("unable to update User Details", error)
    );
    //update userzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
  }
  noteditAddressDetails(){

    console.log(this.edit1Address);
    if(this.edit1Address.addressId!=null){
      this._service.updateAddress(this.edit1Address).subscribe(
        data=>{
          console.log("Address updated");
          this.hideEditingAddress=true;
          this.hideAllExtraOption();
          this.msg="Address Updated Successfully";
          this.showMsg();
          this.getAllMyAddress();
        },
        error=>console.log("unable to update address ", error)
      );
    }
    else{
      this._service.addNewAddress(this.edit1Address).subscribe(
        data=>{
          console.log("Address Added");
          this.hideEditingAddress=true;
          this.hideAllExtraOption();
          this.msg="Address Added Successfully";
          this.showMsg();
          this.getAllMyAddress();
        },
        error=>console.log("unable to add new address ", error)
      );
    }
    
  }
  hideAll(){
    this.hidewishlist=true;
    this.hideMyRating=true;
    this.isShow=true;
    this.isAddressDetailsShow=true;
  }

  getAllMyAddress(){
    this._service.getAllMyAddress().subscribe(
      dataAddr=>{
        console.log("All Addresses",dataAddr);
        if(dataAddr.length==1 && dataAddr[0].addressId==null){

        }
        else{
          this.allAddress=dataAddr;
          this.allAddress.sort(function(a,b){ return a.addressId-b.addressId})
          this.extraOptionMenu=[];
          for(let i=0;i<this.allAddress.length;i++){
            this.extraOptionMenu.push(false);
          }
        }
      }
    )
  }

  copyAddress(copyFrom:Address,copyTo:Address){
    copyTo.address=copyFrom.address;
    copyTo.addressId=copyFrom.addressId;
    copyTo.city=copyFrom.city;
    copyTo.contactNumber=copyFrom.contactNumber;
    copyTo.firstName=copyFrom.firstName;
    copyTo.lastName=copyFrom.lastName;
    copyTo.state=copyFrom.state;
    copyTo.userId=copyFrom.userId;
    copyTo.zipCode=copyFrom.zipCode;
    copyTo.country="India";
    copyTo.middleName=copyFrom.middleName;
    copyTo.current=copyFrom.current;
    return copyTo;
  }

  addNewAddress(){
    this.edit1Address=new Address();
    this.edit1Address.country="India";
    this.edit1Address.current=false;
    this.hideEditingAddress=false;
    this.notEditAddress=false;
  }

  editAddress(address1){
    this.edit1Address=this.copyAddress(address1,this.edit1Address);
    this.hideEditingAddress=false;
    this.notEditAddress=false;
  }

  deleteAddress(address1:Address){
    this._service.deleteAddress(address1.addressId).subscribe(
      data=>{
        if(data.Value=="Deleted"){
          this.msg="Address Deleted Successfully";
          this.showMsg();
          this.getAllMyAddress();
        }
        else{
          this.msg="Address Delete Unsuccessful";
          this.showMsg();
        }
      },
      error=>{
        console.log("address not deleted", error);
        this.msg="Address Delete Unsuccessful..";
        this.showMsg();
      }
    )
  }

  hideAllExtraOption(){
    for(let i=0;i<this.extraOptionMenu.length;i++){
      this.extraOptionMenu[i]=false;
    }
  }

  toggleExtraOptionMenu(i){
    if(this.extraOptionMenu[i]==true){
      this.extraOptionMenu[i]=false;
    }
    else{
      this.hideAllExtraOption();
      this.extraOptionMenu[i]=true;
    }

  }


  constructor(private _service:RegistrationService ,private _router:Router,private route: ActivatedRoute, private serviceProd:AddproductService) { }

  checkLogin(){
    let loginUserRole=window.localStorage.getItem("userRole");
    console.log(loginUserRole);
    if(loginUserRole==null)
    {
      console.log("Please login");
      this._router.navigate(["/login"]);
    }
  }
  

  ngOnInit(): void {
    this.checkLogin();
    let sub = this.route.snapshot.paramMap.get('submenu');

    this.checkRole();
    this.user =new User();
    this.userCopy=new User();
    console.log(this.user);
    this.showPersonalDetails();
    this.getAllMyAddress();
    if (sub=="wishlist"){
      this.hideAll();
      this.hidewishlist=false;
    }
    else if(sub=="addAddress"){
      this.hideAll();
      this.toggleAddressDisplay();
      this.addNewAddress();
    }
      
    // this.fetchAddress();
    //aesdjuyhsadbjgbhsdajhfsadbvjubgfdgsdabbbbbazzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
  }

  goToCart(){
    
  }
  toggle(){

  }

  toggleMenu(){
    this.hideMenu=!this.hideMenu;
  }

  showMenuFn(){
    this.hideMenu=false;
  }

  hideMenuFn(){
    this.hideMenu=true;
    this.hideSubMenu=true;
  }

  fetchAddress(userdata){
    this._service.getAddress().subscribe(
      data=>{
        console.log(data);
        this.user.address=data;
        this.user.id=userdata.id;
        this.user.firstName=userdata.firstName;
        this.user.lastName=userdata.lastName;
        this.user.middleName=userdata.middleName;
        this.user.gender=userdata.gender;
        this.user.primaryContact=userdata.primaryContact;
        this.user.email=userdata.email;
        this.user.alternateContact=userdata.alternateContact;
        this.userCopy=this.userChange(this.user,this.userCopy);
      },
      error=> console.log("Error while fetching address",error)
    )
  }

  toggleDisplay(){
    this.hideAll();
    this.isShow=false;
    this.isAddressDetailsShow=true;
    this.hideMenuFn();
  }
  toggleAddressDisplay()
  {
    this.hideAll();
    this.isShow=true;
    this.isAddressDetailsShow=false;
    this.hideMenuFn();
    this.hideEditingAddress=true;
    this.hideAllExtraOption();
  }

  goToMyOrders(){
    this._router.navigate(["/orderdetails"]);
  }

  goToWishlist(){
    this.hideAll();
    this.hidewishlist=false;
  }
  goToMyRating(){
    this.hideAll();
    this.hideMyRating=false;
  }

  logout(){
    window.localStorage.removeItem('EcomToken');
    window.localStorage.removeItem('userRole');
    this._router.navigate(["/login"]);
  }

  userChange(fromUser:User,toUser:User){
    toUser.firstName=fromUser.firstName;
    toUser.middleName=fromUser.middleName;
    toUser.lastName=fromUser.lastName;
    toUser.primaryContact=fromUser.primaryContact;
    toUser.alternateContact=fromUser.alternateContact;
    toUser.email=fromUser.email;
    return toUser;
  }

  showPersonalDetails(){
    this._service.getUserByEmail().subscribe(
      
      data => {​​
      console.log("response received");
      // this._router.navigate(['/loginSuccess']);
      sessionStorage.setItem("login ","success");
      this.fetchAddress(data);
      this.user=<User>data;
      this.user.roleId=data.role.roleId;
      console.log(this.user);
      this.userCopy=this.userChange(this.user,this.userCopy);
      }​​,
      error =>{​​
      console.log("exception occured");
      this.msg = "Incorrect Credentials,Please enter correct username and password";
      }​​
      
    );
  }


  removeFromWishlist(val){
    this.msg="Are you sure you want to remove this product from your Wishlist?";
    this.showMsgBox2("Delete","Cancel");
  }
  confirmRemove(val){
    this.wishlistCmp.confirmRemove(val);
  }
  rateProdId=null;
  rateProdName=null;
  rateProdHeading=null;
  rateProdRating=null;
  rateProdReview=null;
  showRatingDiv=false;
  editRating(val){
    let id=val.id;
    let name = val.name;
    // this.loader1=false;
    this.rateProdId=id;
    this.rateProdName=name;
    this.serviceProd.getMyRating(id).subscribe(
      ratingData=>{
        console.log(ratingData);
        this.rateProdRating=ratingData.rating;
        this.rateProdReview=ratingData.comment;
        this.rateProdHeading=ratingData.heading;
        // this.loader1=true;
        this.showRatingDiv=true;
      },
      error=>{
        this.rateProdRating=null;
        this.rateProdReview=null;
        this.rateProdHeading=null;
        // this.loader1=true;
        this.showRatingDiv=true;
      }
    )

    // this.rateProdId=null;
  }
  ratingComplete(val){
    this.showRatingDiv=false;
    this.rateProdId=null;
    this.rateProdName=null;
    this.rateProdRating=null;
    this.rateProdReview=null;
    this.rateProdHeading=null;
    console.log("rating value=",val);
    this.myReviewsCmp.getMyRatings();
  }
  

  showMsgFromChild(val){
    this.msg=val;
    this.showMsg();
  }

  showMsg(){
    this.msgDiag.myfunc();
  }
  showMsgBox1(bName?){
    this.msgDiag.showbox1(bName);
  }
  showMsgBox2(b1Name?,b2Name?){
    this.msgDiag.showbox2(b1Name,b2Name);
  }
  msgResult(val){
    this.confirmRemove(val);
  }

  checkRole(){
    this.userRole=window.localStorage.getItem("userRole");
  }
 
}
