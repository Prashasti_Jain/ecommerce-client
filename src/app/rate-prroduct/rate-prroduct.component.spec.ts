import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RatePrroductComponent } from './rate-prroduct.component';

describe('RatePrroductComponent', () => {
  let component: RatePrroductComponent;
  let fixture: ComponentFixture<RatePrroductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RatePrroductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RatePrroductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
