import { Component, EventEmitter, Input, OnInit, Output, ViewChild, ɵConsole } from '@angular/core';
import { MsgDialogueComponent } from '../msg-dialogue/msg-dialogue.component';
import { Review } from '../review';
import { AddproductService } from '../shared/addproduct.service';

@Component({
  selector: 'app-rate-prroduct',
  templateUrl: './rate-prroduct.component.html',
  styleUrls: ['./rate-prroduct.component.css']
})
export class RatePrroductComponent implements OnInit {
  constructor(private service : AddproductService) { }
  @Input() prodId:number;
  @Input() prodName:string;
  @Output() rateEvent = new EventEmitter<any>();
  @Input() rating:number=0;
  @Input() comment:string;
  @Input() heading:string;
  
  ratingFunc(val){
    this.rating=val;
  }
  submitRating(){
    let review = new Review();
    review.productId=this.prodId;
    review.rating=this.rating;
    review.comment=this.comment;
    review.heading=this.heading;
    this.service.addRating(review).subscribe(
      data=>{
        console.log("Product Rated Successfully");
        this.rateEvent.emit(this.rating);
      },
      error=>{
        console.log(error);
        this.rateEvent.emit(-2);
      }
      
    );
  }
  cancel(){
    this.rateEvent.emit(-1);
  }

  ngOnInit(): void {
  }

  func(event){
    event.stopPropagation();
  }
}
