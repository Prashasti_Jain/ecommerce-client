export class Review{
    rating:number;
    reviewId:number;
    productId:number;
    comment:string;
    username:string;
    heading:string;
    date:string;
}