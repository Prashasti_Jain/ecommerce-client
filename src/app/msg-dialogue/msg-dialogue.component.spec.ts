import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MsgDialogueComponent } from './msg-dialogue.component';

describe('MsgDialogueComponent', () => {
  let component: MsgDialogueComponent;
  let fixture: ComponentFixture<MsgDialogueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MsgDialogueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MsgDialogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
