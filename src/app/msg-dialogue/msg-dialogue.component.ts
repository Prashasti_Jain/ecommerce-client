import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ThemeService } from 'ng2-charts';
// import { setTimeout } from 'timers';

@Component({
  selector: 'app-msg-dialogue',
  templateUrl: './msg-dialogue.component.html',
  styleUrls: ['./msg-dialogue.component.css']
})
export class MsgDialogueComponent implements OnInit {

  @Input() msg:string;
  @Output() result = new EventEmitter<any>();

  hideAlert=true;
  hide2box=true;
  hide1box=true;
  buttonName="Ok";
  button1Name="Ok";
  button2Name="Cancel";

  constructor() { }

  ngOnInit(): void {
  }
  myfunc(){
      this.hideAllDiv();
      this.hideAlert=false;
      document.getElementById("main").style.animation="cssAnimation";
      document.getElementById("main").style.animationDuration="1.5s";
      document.getElementById("main").style.animationDelay="2s";
      document.getElementById("main").style.animationFillMode="forwards";
      setTimeout(()=>{
        document.getElementById("main").style.removeProperty("animation");
        this.hideAlert=true;
        },
        3500
      )
  }

  showbox1(buttonName?:string){
    this.hideAllDiv();
    this.buttonName=buttonName;
    this.hide1box=false;
  }

  showbox2(button1Name?:string,button2Name?:string){
    this.hideAllDiv();
    this.button1Name=button1Name;
    this.button2Name=button2Name;
    this.hide2box=false;
  }

  hideAllDiv(){
    this.hideAlert=true;
    this.hide1box=true;
    this.hide2box=true;
  }

  emitResult(i:number){
    this.hideAllDiv();
    this.result.emit(i);
  }

  

  // animation: cssAnimation 1.2s ease-in 2s;
}
