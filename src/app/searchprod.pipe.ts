import { Pipe, PipeTransform } from '@angular/core';
import { Product } from './shared/product';

@Pipe({
  name: 'searchprod'
})
export class SearchprodPipe implements PipeTransform {

  transform(products:Product[],searchVal:string): Product[] {
    if(!products || !searchVal){
      return products;
    }
    return products.filter(product =>
      product.productName.toLocaleLowerCase().includes(searchVal.toLocaleLowerCase()) ||
      product.categoryName.toLocaleLowerCase().includes(searchVal.toLocaleLowerCase()) ||
      product.brandName.toLocaleLowerCase().includes(searchVal.toLocaleLowerCase()));
  }

}
