
import { Category } from './Category';
import { Image } from './Image';

export class Product {
    productName: string;
    productPrice: number;
    productDesc: string;
    productId = null;
    imageIds : string = "";
    image : Image[]=[];
    createdBy: string ="";
    productStatus: string ="Pending";
    productQuantity:number=1;
    color:string;
    brandName:string;
    brandId:number;
    category={"categoryId":null,"categoryName":null};

    categoryId : number;
    categoryName :string = this.category.categoryName;

    cat={0:"NA",1:"Electronics",2:"Books",3:"Fashion",4:"Furniture",5:"Phone"};
    
    constructor(productName?, price?, description?, category?, imageIds?:string){
        this.productName=productName;
        this.productDesc=description;
        this.productPrice=price;
        
        this.category["categoryId"]=category;
        if(category!=null){
            this.category["categoryName"] = this.cat[category];
            this.categoryName=this.cat[category];
        }
        this.categoryId = category;
        if(imageIds!=null){
            this.imageIds = imageIds.replace("null;","");
        }
        this.productStatus="Pending";
        this.createdBy="abc@gmail.com"
    }

}