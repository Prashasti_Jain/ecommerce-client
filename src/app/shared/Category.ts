export class Category {

    categoryName: string;
    categoryId: number;

    constructor(name?,id?){
        this.categoryName=name;
        this.categoryId=id;
    }

}