import { Category } from './Category';
import { Image } from './Image';

export class Product {
    productName: string;
    productPrice: string;
    productDesc: string;
    productId = null;
    imageId : string = "";
    image:Image[]=[];
    createdBy: string;
    productStatus: string;
    productQuantity:number=1;

    category={"categoryId":null,"categoryName":null};

    categoryId : number;

    cat={0:"NA",1:"Electronics",2:"Books",3:"Fashion",4:"Furniture",5:"Phone"};
    
    constructor(productName?, price?, description?, category?, imageId?:string, createdBy?, productStatus?){
        this.productName=productName;
        this.productDesc=description;
        this.productPrice=price;
        this.category["categoryId"]=category;
        if(category!=null){
        this.category["categoryName"] = this.cat[category];
        }
        this.categoryId = category;
        if(imageId!=null){
        this.imageId = imageId.replace("null;","");
        }
        this.createdBy=createdBy;
        this.productStatus=productStatus;
    }

}