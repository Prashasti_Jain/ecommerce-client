export class Addproduct {
    productID: number = null;
    productName: string;
    category: string;
    brand: number;
    price: number;
    color: string;
    description: string;
    imageId : string;
    productStatus : string;
    createdBy : string;
    createdOn : Date;
    modifiedOn : Date;
    modifiedBy : string;
    vId: number;
    productQuantity:number=1;
    constructor(productName?,category?, price?, description?){
        this.productName=productName;
        this.description=description;
        this.price=price;
        this.imageId=null;
        this.category=category;
        this.productStatus="pending";
        this.createdBy="";
        this.createdOn=null;
    }
}
