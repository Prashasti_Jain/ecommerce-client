import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Product } from './product.model';
import { PortUrl } from '../portUrl';
import { Cartmodel } from '../cartmodel';

@Injectable({
  providedIn: 'root'
})
export class cartService {

  //Allproduct=new Product();
  constructor(private _http: HttpClient) { }
  private portUrl = PortUrl.portUrlProduct;
  _url = this.portUrl+'cartItemsofUser';
  _url_allCartProd = this.portUrl+'allProductsById'; 
  _urlDelete = this.portUrl+'deleteById/';
  _urlUpdate = this.portUrl+'updateCart/';
  _url_allOrder = this.portUrl + 'allOrders';


  getAllProducts(arr : number[]){
    console.log(arr);
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.post<any>(this._url_allCartProd,arr,{headers:headers_object});
  }

  getAll(){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    console.log("fsdgsdfghsdffsd");
    return this._http.get<any>(this._url,{headers:headers_object});
  }

  getAllOrders(){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    // console.log("fsdgsdfghsdffsd");
    return this._http.get<any>(this._url_allOrder,{headers:headers_object});
  }

  removeFromCart(id){
    
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    let urlDel=this._urlDelete + id;
    return this._http.delete(urlDel, {headers:headers_object});  
  }

  changeQty(id,prod:Cartmodel){
    
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    let urlUpd=this._urlUpdate + id;
    //return this._http.put<any>(this._urlUpdate,prod); //need to add in backend
    return this._http.put<any>(urlUpd,prod.quantity,{headers:headers_object});
  }
}
