import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ThemeService } from 'ng2-charts';
import { Observable } from 'rxjs';
import { PortUrl } from 'src/app/portUrl';
import { Brand } from '../brand';
import { Cartmodel } from '../cartmodel';
import { Order } from '../order';
import { Review } from '../review';
import { Addproduct } from './addproduct.model';
import { Category } from './Category';
import { Product } from './product';

@Injectable({
  providedIn: 'root'
})
export class AddproductService {

  private porturl = PortUrl.portUrlProduct;
  formData: Addproduct;
  _url = this.porturl + 'addProduct';
  // _urlSearch = this.porturl + 'product/';
  _urlSearch = this.porturl + 'allProduct/';
  _url_img = this.porturl + 'storage/uploadFile';
  _url_createdBy = this.porturl + 'allProductCreatedBy';
  _url_delete = this.porturl + 'delete/';
  _url_searchByid = this.porturl + 'productById/';
  _url_allProducts = this.porturl + 'products';
  _url_updateProductStatus = this.porturl + 'updateProductStatus';
  
  _url_deleteImage = this.porturl + 'deleteImage';

  _url_updateProduct = this.porturl + 'updateProduct/';

  _url_rateProduct = this.porturl + 'rateProduct';

  _url_addToCart = this.porturl + 'addProductsToCart';

  _url_addOrder = this.porturl + 'saveorders';
  
  _url_addReview = this.porturl + 'addReview';

  _url_getMyReview = this.porturl + 'getAlreadyRated/';

  _url_getRating = this.porturl + 'getAvgRating/'

  _url_getCategory = this.porturl + 'getCategory'

  _url_addCategory = this.porturl + "addCategory";

  _url_getBrand = this.porturl + 'getBrand';

  _url_addBrand = this.porturl + "addBrand";

  _url_orders = this.porturl + "orders";

  _url_allreviews = this.porturl + "getReviewByPid/";

  _url_getAllMyReview = this.porturl + "getReviewByUsername";

  _url_deleteReview = this.porturl + "removeReview/"
    
  _url_myWishlist = this.porturl + "wishListItemsofUser";

  _url_addToWishlist = this.porturl + "addProductsToWishList";

  _url_deleteFromWishlist = this.porturl + "removeFromWishlist/"


  // userName="abc@gmail.com";

  constructor(private _http: HttpClient) { }

  //sending product to server : 1
  sendProd(){
    
    // let tokenS=window.localStorage.getItem('EcomToken');
    // let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    let tempProd = new Product(this.formData.productName,this.formData.price,this.formData.description, this.formData.category, this.formData.imageId);
    // console.log(this.formData);
    tempProd.productQuantity=this.formData.productQuantity;
    // tempProd.productQuantity=this.formData.productQuantity;
    tempProd.brandId=this.formData.brand
    tempProd.color=this.formData.color;
    console.log("this is sending", tempProd);
    this.tempSend(tempProd).subscribe(
      data => {console.log("Success !!! ", data)
        window.alert("Product is Registered and pending for Admin Approval") 
      },
      error => console.log("Error !!!!! ", error)
    ) ;

  }

  //sending product to server : 2
  tempSend(tp){
    
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.post<any>(this._url,tp,{headers:headers_object});
  }
  

  updateOldProd2(updatedProd: Product,){
    // if(deletedImg.length!=0){
      // this.deleteImage(deletedImg).subscribe(
      //   data=> {
          
          let tokenS=window.localStorage.getItem('EcomToken');
          let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
          return this._http.put(this._url_updateProduct+updatedProd.productId,updatedProd,{headers:headers_object})
      //   },
      //   error=>{
      //     console.log(error);
      //     return false;
      //   }
      // )
    // }
  }

  deleteImage(arr: number[]){
    console.log(arr);
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.post<any>(this._url_deleteImage,arr,{headers:headers_object})
  }

  //Search product by name service
  pName='';
  sendSearch(pName){
    this.pName=this._urlSearch + pName;
    // window.alert(this.pName);
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.get<any>(this.pName,{headers:headers_object});

  }


  ///////////////////////////////////////.....................................need to change this
  //Search product by Created by service
  sendCreatedBy(){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    let urlCreatedBy=this._url_createdBy;
    return this._http.get<any>(urlCreatedBy,{headers:headers_object});
  }

  //Upload image to aws server
  uploadImg(files:File[]){
    
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    console.log("going to upload");
    let uploadData = new FormData();
    for(let i=0;i<files.length;i++){
      uploadData.append('file', files[i], files[i].name);
    }
    // this._http.post('my-backend.com/file-upload', uploadData)
    return this._http.post<any>(this._url_img,uploadData,{headers:headers_object});
  }

  //delete product with id
  deleteProduct(idDel){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    let urlDel=this._url_delete + idDel;
    return this._http.delete<any>(urlDel,{headers:headers_object});
  }

  sendSearchById(id){
    
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    // console.log("sdgsdgsgdd")
    let urlId = this._url_searchByid+id;
    return this._http.get<any>(urlId,{headers:headers_object});
  }

  getAllProduct(){
    
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.get<any>(this._url_allProducts,{headers:headers_object});
  }

  addOrder(orders:Order[]){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.post<any>(this._url_addOrder,orders,{headers:headers_object});
  }

  addProdToCart(cart:Cartmodel){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.post<any>(this._url_addToCart,cart,{headers:headers_object});
  }

  addRating(review:Review){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.post<any>(this._url_addReview,review,{headers:headers_object});
  }
  getMyRating(pid:number){
    let temp_url= this._url_getMyReview+pid;
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.get<any>(temp_url,{headers:headers_object});
  }
  
  getAvgRating(pid:number){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    let url_new = this._url_getRating+pid;
    return this._http.get<any>(url_new,{headers:headers_object});
  }

  updateProductStatus(prod:Product){
    
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    let id_stat:number[]=[];
    id_stat.push(prod.productId);
    if(prod.productStatus=="Approved"){
      id_stat.push(1);
    }
    else{
      id_stat.push(-1);
    }
    return this._http.put<any>(this._url_updateProductStatus,id_stat,{headers:headers_object});
  }

  getMyOrders(){
    
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.get<any>(this._url_orders,{headers:headers_object});

  }

  getAllCategories(){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    console.log(headers_object);
    return this._http.get<any>(this._url_getCategory,{headers:headers_object});
  }
  addNewCategory(cat:string){
    let newCat = new Category();
    newCat.categoryName=cat;
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    console.log(headers_object);
    return this._http.post<any>(this._url_addCategory,newCat,{headers:headers_object});
  }

  getAllBrands(){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    console.log(headers_object);
    return this._http.get<any>(this._url_getBrand,{headers:headers_object});
  }

  addNewBrand(cat:string){
    let newBrand = new Brand();
    newBrand.brandName=cat;
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    // console.log(headers_object);
    return this._http.post<any>(this._url_addBrand,newBrand,{headers:headers_object});
  }

  getAllReviews(id){
    let tempUrl= this._url_allreviews+id;
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.get<any>(tempUrl,{headers:headers_object});
  }

  getAllMyReviews(){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.get<any>(this._url_getAllMyReview,{headers:headers_object});
  }

  deleteMyReview(pid:number){
    let tempUrl = this._url_deleteReview + pid; 
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.delete<any>(tempUrl,{headers:headers_object});
  }

  addToWishlist(id:number){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.post<any>(this._url_addToWishlist,id,{headers:headers_object});
  }

  getMyWishlist(){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.get<any>(this._url_myWishlist,{headers:headers_object});
  }

  deleteFromWishlist(pid:number){
    let temp_url = this._url_deleteFromWishlist + pid;
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.delete<any>(temp_url,{headers:headers_object});
  }
}
