import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Product } from '../shared/product.model';
import { PortUrl } from 'src/app/portUrl';

@Injectable({
  providedIn: 'root'
})
export class HomepageService {

  //Allproduct=new Product();
  private portUrl = PortUrl.portUrlProduct;
  constructor(private _http: HttpClient) { }

  _url= this.portUrl + 'productsApproved';
  _urlSearch = this.portUrl + 'allProduct/';
  getAll(){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.get<any>(this._url,{headers:headers_object});
  }

  goToCart() {
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.get<any>(this._url,{headers:headers_object});
  }

  pName='';
  sendSearch(pName){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    this.pName=this._urlSearch + pName;
    //window.alert(this.pName);
    return this._http.get<any>(this.pName,{headers:headers_object});
  }

  tempSend(tp){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    return this._http.post<any>(this._url,tp,{headers:headers_object});
  }
  
}
