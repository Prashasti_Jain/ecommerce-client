import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Address } from '../address';
import { MsgDialogueComponent } from '../msg-dialogue/msg-dialogue.component';
import { Order } from '../order';
import { RegistrationService } from '../registration.service';
import { AddproductService } from '../shared/addproduct.service';
import { Awsurl } from '../shared/awsurl';
import { cartService } from '../shared/cart.service';
import { Product } from '../shared/product';
import { User } from '../user';

@Component({
  selector: 'app-placeorder',
  templateUrl: './placeorder.component.html',
  styleUrls: ['./placeorder.component.css']
})
export class PlaceorderComponent implements OnInit {

  @ViewChild(MsgDialogueComponent) msgDiag:MsgDialogueComponent;
  msg2="";
  constructor(private serviceCart: cartService,private serviceAddprod:AddproductService,
    private _service:RegistrationService,private _router: Router ) { }

  
  awsurl = Awsurl.awsUrl;
  user:User;
  userRole;
  msg = '';
  isShow=false;
  isSearch=true;
  btnshow=true;
  noProd=true;
  showCreatedBy=true;
  noProdCreatedBy=true;
  searchId1;
  allOrders:Order[]=[];
  prodIds:number[]=[];
  allProducts:Product[]=[];
  allAddress:Address[]=[];
  showAddress:Address[]=[];
  isShown: boolean = false ;
  hideAddrDiv = false;
  hideOrderDiv = true;
  hidePaymentDiv = true;
  addressDone=false;
  orderDone=false;
  totalAmount=0;
  totalItems=0;
  deliverAddress:Address=new Address;
  paymentMethod:string="COD";
  
  toggleShow() {
    this.isShown = ! this.isShown;
  }

  deliverHere(){
    this.hideAllSub();
    this.hideOrderDiv=false;
    this.addressDone=true;
  }
  
  continueOrder(){
    this.hideAllSub();
    this.hidePaymentDiv=false;
    this.orderDone=true;
  }

  hideAllSub(){
    this.hideAddrDiv = true;
    this.hideOrderDiv = true;
    this.hidePaymentDiv = true;
  }

  copyAddr(){
    this.showAddress=[];
    for(let k=0;k<this.allAddress.length;k++){
      if(this.allAddress[k].current){
        this.showAddress.push(this.allAddress[k]);
        this.deliverAddress=this.allAddress[k];
      }
    }
    for(let i=0;i<this.allAddress.length;i++)
    {
      if(i>0)
        break;
      if(!this.allAddress[i].current){
        this.showAddress.push(this.allAddress[i]);
      }

    }
    console.log(this.showAddress);
  }

  extendAddress(){
    this.showAddress=[];
    for(let k=0;k<this.allAddress.length;k++){
      if(this.allAddress[k].current){
        this.showAddress.push(this.allAddress[k]);
        this.deliverAddress=this.allAddress[k];
      }
    }
    for(let i=0;i<this.allAddress.length;i++)
    {
      if(!this.allAddress[i].current)
        this.showAddress.push(this.allAddress[i]);
    }
  }
  addAddressInUser(){
    this._router.navigate(["/userprofile","addAddress"]);
  }


  fetchAddress(){
    this._service.getUserByEmail().subscribe(
      dataUser=>{
        this.user=dataUser;
        this._service.getAllMyAddress().subscribe(
          data=>{
            this.allAddress=[];
            if(data!=null && data.length!=0)
            {
              if(data.length==1 && data[0].addressId==null){

              }
              else{
                for(let i=0;i<data.length;i++){
                  this.allAddress.push(data[i]);
                }
                this.copyAddr();
                console.log(this.allAddress);
                // console.log(data);
              }
            }

          },
          error=> console.log("Error while fetching address",error)
        )
      },
      error=>console.log(error)
    )
    
  }

  toggle(){
    
    this.isShow=false;
    this.isSearch=true;
  }
  goToCart() {
    this._router.navigate(['/cart']);
  }
  
  placeOrder(Placeorderform:NgForm)
  {
    this._service.updateAddress(this.user.address).subscribe(
      data=>{
        for(let i=0;i<this.allOrders.length;i++){
          this.allOrders[i].addressId=data.addressId;
        }
        console.log("Address updated")
        console.log(this.allOrders);

        this.serviceAddprod.addOrder(this.allOrders).subscribe(
          data=>{
            // window.alert("Order placed successfully");
            this.msg2="Order placed successfully";
            this.showMsg();
            
            console.log(data);
          },
          error=>{
            // window.alert("Could not place Order");
            this.msg2="Could not place Order";
            this.showMsg();
            console.log(error);
          }
        )

        
      },
      error=>console.log("unable to update address ", error)
    );
    // alert("Order Placed Successfully....! ");
    
    this._router.navigate(['/homepage']);

  }

  msgBoxReply(){
    this._router.navigate(['/homepage']);
  }

  placeOrderNew(){
    for(let i=0;i<this.allOrders.length;i++){
      this.allOrders[i].addressId=this.deliverAddress.addressId;
    }
    this.serviceAddprod.addOrder(this.allOrders).subscribe(
      data=>{
        // window.alert("Order placed successfully");
        this.msg2="Order placed successfully";
        this.showMsgBox1("Ok");
        console.log(data);
      },
      error=>{
        // window.alert("Could not place Order");
        this.msg2="Could not place Order";
        this.showMsgBox1("Ok");
        console.log(error);
      }
    )

  }

  toggleDisplay(){
    this.isShow=false;
    this.isSearch=true;
    this.btnshow=false;
    this.noProd=true;
    this.showCreatedBy=true;
    this.noProdCreatedBy=true;
    this.searchId1='';
  }
  newAddress(){
    this.user.address.address=null;
    this.user.address.zipCode=null;
    this.user.address.firstName=null;
    this.user.address.lastName=null;
    this.user.address.city=null;
    this.user.address.state=null;
    this.user.address.contactNumber=null;
  }

  getAllCartItems(){
    this.serviceCart.getAll().subscribe(
      data=>{
        console.log(data);
        this.allOrders=[];
        this.totalAmount=0;
        this.totalItems=0;
        for(let i=0;i<data.length;i++){
          let order = new Order();
          order.addressId=data[i].addressId;
          order.productId=data[i].product.productId;
          order.quantity=data[i].quantity;
          order.totalAmount=data[i].totalPrice;
          console.log(order);
          this.totalAmount+=data[i].totalPrice;
          this.totalItems+=data[i].quantity;
          if(order.quantity!=0){
            this.allOrders.push(order);
            this.prodIds.push(order.productId);
          }
        }
        this.serviceCart.getAllProducts(this.prodIds).subscribe(
          data=>{
            this.allProducts=data;
            for(let ii=0;ii<this.allProducts.length;ii++){
              this.allProducts[ii].image[0].imageURL=this.awsurl+this.allProducts[ii].image[0].imageURL;
              this.allProducts[ii].createdBy=data[ii].sellerName;
            }
            console.log(data);
          },
          error=>console.log("could not fetch Products")
        );
      },
      error=>console.log("could not get cart items",error)
    )
  }

  isShowDiv = false;
   
  toggleDisplayDiv(val:string) {
    if(val=='address')
    {
      this.isShowDiv=false;
    }
    else{
      this.isShowDiv=true;
    }
  }

  checkLogin(){
    let loginUserRole=window.localStorage.getItem("userRole");
    console.log(loginUserRole);
    if(loginUserRole==null)
    {
      console.log("Please login");
      // this.msg="Please Login";
      // this.showMsgBox1("Login");
      this._router.navigate(["/login"]);
    }
  }

  ngOnInit(): void {
    this.checkLogin();
    this.checkRole();
    this.isShowDiv = false;
    this.user = new User();
    // this.user.address = new Address();
    console.log(this.user);
    this.fetchAddress();
    this.getAllCartItems();
    
  }
  checkRole(){
    this.userRole=window.localStorage.getItem("userRole");
  }
  showMsg(){
    this.msgDiag.myfunc();
  }
  showMsgBox1(bName?){
    this.msgDiag.showbox1(bName);
  }

}
