
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { RegistrationService } from '../registration.service';
import { User } from '../user';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user = new User();
  msg = '';
  accessToken = '';
  warningHide = true;
  constructor(private _service: RegistrationService, private _router: Router) { }


  checkLogin(){
    let loginUserRole=window.localStorage.getItem("userRole");
    console.log(loginUserRole);
    if(loginUserRole!=null)
    {
      console.log("You are already Logged in");
      this._router.navigate([""]);
    }
  }
  

  ngOnInit(): void {
    // window.localStorage.removeItem('EcomToken');
    // window.localStorage.removeItem('userRole');
    this.checkLogin();
    this.warningHide = true;
  }

  loginUser() {


    this._service.TokenGen(this.user).subscribe(
      data => {
        // console.log("response received", data);
        // this.accessToken = data.access_token;
        // this._service.accessTokenS = data.access_token;
        window.localStorage.setItem('EcomToken', data.access_token);
        console.log("Token = ", window.localStorage.getItem('EcomToken'));

        this._service.getUserByEmail().subscribe(
          data2 => {
            // console.log(data2);
            window.localStorage.setItem("userRole", data2.role.roleId);
            window.localStorage.setItem("userFName", data2.firstName);
            if (data2.role.roleId == 1) {
              this._router.navigate(['/homepage']);
            }
            else if (data2.role.roleId == 2) {
              this._router.navigate(['/addProduct']);
            }
            else if (data2.role.roleId == 3) {
              this._router.navigate(['/admin']);
            }
          },
          error => console.log("emailerror ", error)
        );
        sessionStorage.setItem("login ", "success")
      },
      error => {
        console.log("exception occured", error);
        this.msg = "Incorrect Credentials, or Unauthorised User";
        this.warningHide = false;
      }

    );

  }
  gotoregisteration() {
    console.log("Hi Welcome to E-commerce App");
    this._router.navigate(['/register']);

  }
  forgotPassword() {
    this._router.navigate(['/forgotpwd']);
  }
}
