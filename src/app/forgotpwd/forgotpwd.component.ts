import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { RegistrationService } from '../registration.service';
import { User } from '../user';



@Component({
  selector: 'app-forgotpwd',
  templateUrl: './forgotpwd.component.html',
  styleUrls: ['./forgotpwd.component.css']
})
export class ForgotpwdComponent implements OnInit {

  user = new User();
  optionForgetpwd= false;

  msg = '';
  selectedQue : "";
  constructor(private _service: RegistrationService, private _router: Router) { }

  ngOnInit(): void {
  }

  onSubmit(buttonType): void {
    if(buttonType==="Send OTP") {

      this._service.sendOTP(this.user).subscribe(
        data=>{
          console.log("OTP generated successfully",data);
          // this._router.navigate(['/login']);
          this.msg="OTP generated successfully";
         
          
        },
        error =>{
          console.log("User not found");
          this.msg="User not found";
        }
      );
    }
    if(buttonType==="Set Password"){
      this._service.forgetPasswordwithOTP(this.user).subscribe(
        data=>{
          console.log("Password changed successfully",data);
          // this._router.navigate(['/login']);
          this.msg="Password changed successfully";
          this.user.otp="";
          this.user.password="";
  
        },
        error =>{
          console.log("Could not change password");
          this.msg="Could not change password";
        }
      );
    }

}
 
  // resetPassword(OTPform: NgForm) {
  //   alert("Password Changed Successfully.Please login with new password");
  //   OTPform.resetForm();
  //   this._router.navigate(['/login']);
  // }
  sendOTP()
  {
    this._service.sendOTP(this.user).subscribe(
      data=>{
        console.log("OTP generated successfully");
        // this._router.navigate(['/login']);
        this.msg="OTP generated successfully";
      },
      error =>{
        console.log("User not found");
        this.msg="User not found";
      }
    );
  }
  submitOTP() {
    this._service.forgetPasswordwithOTP(this.user).subscribe(
      data=>{
        console.log("Password changed successfully");
        // this._router.navigate(['/login']);
        this.msg="Password changed successfully";

      },
      error =>{
        console.log("Could not change password");
        this.msg="Could not change password";
      }
    );
  }
  resetPwdwithsecques()
  {
    this._service.forgetPasswordwithsecurityQues(this.user).subscribe(
      data=>{
        console.log("response received with security answer");
        this.msg="response received with security answer";
        this.user.password="";
        this.user.securityAnswer="";
        this.user.email="";
      },
      error =>{
        console.log("Answer doesn't match");
        this.msg="Answer doesn't match";
      }
    );
  }
  gotoregisteration() {
    console.log("Hi Welcome to E-commerce App");
    this._router.navigate(['/register']);

  }
  forgotPassword() {
    this._router.navigate(['/forgotpwd']);
  }
  selectChangeHandler(event:any){
    this.selectedQue =event.target.value;
      }
}