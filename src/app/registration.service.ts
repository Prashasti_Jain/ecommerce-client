import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './user';
import {HttpClient, HttpHeaders} from '@angular/common/http';
// import {Headers} from '@angular/http';
import { PortUrl } from 'src/app/portUrl';
import { Address } from './address';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  private portUrl = PortUrl.portUrlRegistration;

  accessTokenS='';


  constructor( private _http : HttpClient) { }
  
  public loginUserFromRemote(user : User):Observable<any>{
    let url = this.portUrl + "login";
    this.TokenGen(user).subscribe(
      data => {
        console.log(data);
      },
      error => console.log("Adfgskh", error)
    )

    return this._http.post<any>(url,user);
  }

  
  public TokenGen(user: User)
  {
    let headerForToken = new HttpHeaders({'Content-Type':'application/x-www-form-urlencoded', 'Authorization':'Basic ZWNtQ2xpZW50SWQ6ZWNtQ2xpZW50U2VjcmV0'});
    // let data = "{'grant_type': 'password', 'username':'" + user.email + "','password':'" + user.password +"'}";
    let data = "grant_type=password&username=" + user.email + "&password=" + user.password;
    // console.log(user.password);
    let url = this.portUrl + "oauth/token";
    return this._http.post<any>(url,data,{headers:headerForToken});
  }





  public registerUserFromRemote(user :User):Observable<any>{
    console.log(user);
    let url = this.portUrl + "registeruser";
    return this._http.post<any>(url,user);
  }
  public sendOTP(user: User):Observable<any>{
    let url = this.portUrl + "sendOTP/"+user.email;
    return this._http.post<any>(url,user);
  }
  public forgetPasswordwithOTP(user:User):Observable<any>{
    let url = this.portUrl + "forgotPasswordWithOTP";
    return this._http.post<any>(url,user);
  }
  public forgetPasswordwithsecurityQues(user:User):Observable<any>{
    let url = this.portUrl + "forgotpasswordwithSecurityQuestion";
    return this._http.post<any>(url,user);
  }

//djkhfbgsjkehfbgsdjbhg

  public getUserByName(name):Observable<any>{
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    let url = this.portUrl + "findUserByName";
    return this._http.post<any>(url,name,{headers:headers_object});
  }

  public updateUserDetails(user:User):Observable<any>{
    
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    let url = this.portUrl + "updateUserdetails "
    return this._http.post<any>(url,user,{headers:headers_object})
  }

  public getUserByContact(contact ):Observable<any>{
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    let url = this.portUrl + "findUserByContact"
    return this._http.post<any>(url,contact,{headers:headers_object});
  }




  public getUserByEmail():Observable<any>{
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    // console.log(tokenS);
    let url = this.portUrl + "findUserByEmail";
    return this._http.get<any>(url,{headers:headers_object});
  }

  public getAddress():Observable<any>{
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    // console.log(tokenS);
    let url = this.portUrl + "getAddress";
    return this._http.get<any>(url,{headers:headers_object});
  }

  public getAllMyAddress(){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    // console.log(tokenS);
    let url = this.portUrl + "allMyAddress";
    return this._http.get<any>(url,{headers:headers_object});
  }

  public addNewAddress(address:Address):Observable<any>{
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    // console.log(tokenS);
    let url = this.portUrl + "saveNewAddress";
    return this._http.post<any>(url,address,{headers:headers_object});
  }

  public updateAddress(address:Address):Observable<any>{
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    // console.log(tokenS);
    let url = this.portUrl + "updateMySingleAddress";
    return this._http.post<any>(url,address,{headers:headers_object});
  }


  public deleteAddress(addressId:number):Observable<any>{
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    // console.log(tokenS);
    let url = this.portUrl + "deleteMyAddressById/" + addressId;
    return this._http.delete<any>(url,{headers:headers_object});
  }

  public getUserByUserName():Observable<any>{
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    // console.log(tokenS);
    let url = this.portUrl + "findUserByUsername";
    return this._http.get<any>(url,{headers:headers_object});
  }




  //change the below method
  public getAllUser():Observable<any>{
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    let url = this.portUrl + "allUsers";
    return this._http.get<any>(url,{headers:headers_object});
  }

  public updateUser(user,val){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    let url;
    if(val=="deny")
      url = this.portUrl + "denyUser"
    else
      url = this.portUrl + "adminApproval";
    return this._http.put<any>(url, user,{headers:headers_object});
  }
  

  getMyOrderAddress(addrList:number[]){
    let tokenS=window.localStorage.getItem('EcomToken');
    let headers_object = new HttpHeaders().set("Authorization", "Bearer " + tokenS);
    let _url_myOrderAddresses = this.portUrl + "getMyAddresses";
    return this._http.post<any>(_url_myOrderAddresses,addrList,{headers:headers_object});
  }

  handleError(error:Response){}
}
