import { Component, OnInit, ViewChild } from '@angular/core';
import { FooterComponent } from '../footer/footer.component';
import { RegistrationService } from '../registration.service';
import { AddproductService } from '../shared/addproduct.service';
import { Product } from '../shared/product';
import { User } from '../user';
// import { ChartsModule } from 'ng2-charts/ng2-charts';
import { Chart } from 'node_modules/chart.js';

// import * as CanvasJS from 'canvasjs'
import { HttpClient } from '@angular/common/http';
// import { HttpErrorResponse } from '@angular/common/http';
// import * as FusionCharts from "fusioncharts";
import { cartService } from '../shared/cart.service';
import { Order } from '../order';
import { Category } from '../shared/Category';
import { MsgDialogueComponent } from '../msg-dialogue/msg-dialogue.component';
import { Brand } from '../brand';
import { Router } from '@angular/router';


@Component({
  selector: 'app-admin-approval',
  templateUrl: './admin-approval.component.html',
  styleUrls: ['./admin-approval.component.css']
})
export class AdminApprovalComponent implements OnInit {
  @ViewChild(MsgDialogueComponent) msgDiag:MsgDialogueComponent;
  msg2="";
  // screenHeight;
  productsReceived: Array<Product>;
  imgSrc;
  arrimg: string[];
  totalRecords: Number;
  page: Number=1;   
  Price: any=0;
  otherCharges: any=0
  totalPrice: any=0;
  quantity: number;
  count: any=0;
  loader1=true;
  initial: number=0;
  // Label = ["Electronics","Books","Fashion","Furniture","Phone"];  
  Label:string[] =[];
  Data:string[] = [];  
  Linechart = <any>[];
  chart = [];
  unique = [];
  allCategories:Category[]=[];
  allBrands:Brand[]=[];

  
//////////////////////////////////////////////

  users: User[]=[];
  products: Product[]=[];
  orders: Order[]=[];
  // orders: Orders[];
  hideDashBoard=true;
  hideClientList=true;
  hideProductList=true;
  hideOrderList=true;
  hideCategory=true;
  hideLoader=false;
  hideSubtab=false;
  searchProdName: string ="";
  searchProdCategory: string ="";
  searchProdSeller: string ="";
  searchProdStatus: string ="";
  searchUserName: string ="";
  searchUserContact: string ="";
  searchUserEmail: string ="";
  searchUserStatus: string ="";
  btn1color="#0086d3";
  btn2color="#f1f1f1";
  txt1clr="#f2f2f2";
  txt2clr="black";
  newCategory:string="";
  newBrand:string="";
  dashclr="#8992a1";
  clientclr="#8992a1";
  prodclr="#8992a1";
  orderclr="#8992a1";
  catclr="#8992a1";
  dashtxtclr="#fff";
  clienttxtclr="#2874f0";
  prodtxtclr="#2874f0";
  ordertxtclr="#2874f0";
  cattxtclr="#2874f0";
  
 

  hideAlltabs(){  
    this.hideDashBoard=true;
    this.hideClientList=true;
    this.hideProductList=true;
    this.hideOrderList=true;
    this.hideLoader=false;
    this.hideCategory=true;
    this.dashtxtclr="#2874f0";
    this.clienttxtclr="#2874f0";
    this.prodtxtclr="#2874f0";
    this.ordertxtclr="#2874f0";
    this.cattxtclr="#2874f0";
  }

  searchStatus2(event){
    this.searchUserStatus=event.target.value;
  }
  searchStatus(event){
    this.searchProdStatus=event.target.value;
  }

  addNewCategory(){
    if(this.newCategory!=null && this.newCategory!=""){
      let tempName:string;
      let alreadyExists:boolean=false;
      for(let i=0;i<this.allCategories.length;i++){
        tempName=this.allCategories[i].categoryName;
        if(this.newCategory.toLowerCase()==tempName.toLowerCase()){
          alreadyExists=true;
          break;
        }
      }
      if(!alreadyExists){
        this.serviceAddProduct.addNewCategory(this.newCategory).subscribe(
          data=> {
            // window.alert("Data Added Please Refreash this page");
            this.msg2="Category Added Successfully";
            this.showMsg();
            this.newCategory="";
            this.openTab("Category");
          },
          error=> console.log("Unable to add category", error)
        )
      }
      else{
        this.msg2="Catrgory '" + this.newCategory + "' already exists";
        this.showMsgBox1("Ok");
        this.newCategory='';
      }

    }
  }

  addNewBrand(){
    if(this.newBrand!=null && this.newBrand!=""){
      console.log(this.newBrand);
      let tempName:string;
      let alreadyExists:boolean=false;
      console.log(alreadyExists);
      for(let i=0;i<this.allBrands.length;i++){
        tempName=this.allBrands[i].brandName;
        console.log("sadf",this.newBrand," ",tempName)
        if((this.newBrand.toLowerCase())==(tempName.toLowerCase())){
          alreadyExists=true;
          break;
        }
      }
      console.log(alreadyExists,"again");
      if(!alreadyExists){
        this.serviceAddProduct.addNewBrand(this.newBrand).subscribe(
          data=> {
            // window.alert("Data Added Please Refreash this page");
            this.msg2="Brand Added, Please Refreash this page";
            this.showMsg();
            this.newBrand="";
            this.openTab("Category");
          },
          error=> console.log("Unable to add Brand", error)
        )
      }
      else{
        this.msg2="Brand '" + this.newBrand + "' already exists";
        this.showMsgBox1("Ok");
        this.newBrand='';
      }
    }
  }

  // checkStatus(stts){
  //   if(stts=="pending" || stts=="Pending"){
  //     return true;
  //   }
  //   return false;
  // }

  approveProduct(prod: Product){
    // console.log("approve",prod);
    prod.productStatus="Approved";
    this.serviceAddProduct.updateProductStatus(prod).subscribe(
      data => {
        console.log(data);
        // let d1 = <Product> data;
        // if(d1.productStatus!="Approved"){
        //   prod.productStatus="Pending";
        //   window.alert("Unable to Approve the Product, Please refresh the page");
        // }
        // window.alert("Product Approved, Please refresh the page");
        this.msg2="Product Approved, Please refresh the page"
        this.showMsg();
      },
      error => {
        console.log(error);
        prod.productStatus="Pending";
        // window.alert("Unable to Approve the Product, Please refresh the page");
        this.msg2="Unable to Approve the Product, Please refresh the page";
        this.showMsg();
      }
    );
  }
  
  denyProduct(prod){
    // console.log("reject",prod);
    prod.productStatus="Denied";
    this.serviceAddProduct.updateProductStatus(prod).subscribe(
      data => {
        console.log(data);
        // let d1 = <Product> data;
        // if(d1.productStatus!="Denied"){
        //   prod.productStatus="Pending";
        //   window.alert("Unable to Deny the Product, Please refresh the page");
        // }
        // window.alert("Product Denied, Please refresh the page");
        this.msg2="Product Denied, Please refresh the page";
        this.showMsg();
      },
      error => {
        console.log(error);
        prod.productStatus="Pending";
        // window.alert("Unable to Deny the Product, Please refresh the page");
        this.msg2="Unable to Deny the Product, Please refresh the page";
        this.showMsg();
      }
    );
  }

  approveUser(user: User){
    user.kycStatus="Approved";
    this.service.updateUser(user,"approve").subscribe(
      data => {
        console.log(data);
        let d1 = <User> data;
        if(d1.kycStatus!="Approved"){
          user.kycStatus="Pending";
          // window.alert("Unable to Approve the User, Please refresh the page");
          this.msg2="Unable to Approve the User, Please refresh the page";
          this.showMsg();
          
        }
      },
      error => {
        console.log(error);
        user.kycStatus="Pending";
        // window.alert("Unable to Approve the User, Please refresh the page");
        this.msg2="Unable to Approve the User, Please refresh the page";
        this.showMsg();
      }
    );
  }

  denyUser(user){
    user.kycStatus="Denied";
    this.service.updateUser(user,"deny").subscribe(
      data => {
        console.log(data);
        let d1 = <User> data;
        if(d1.kycStatus!="Denied"){
          user.kycStatus="Pending";
          // window.alert("Unable to Deny the User, Please refresh the page");
          this.msg2="Unable to Deny the User, Please refresh the page";
          this.showMsg();
        }
      },
      error => {
        console.log(error);
        user.kycStatus="Pending";
        // window.alert("Unable to Deny the User, Please refresh the page");
        this.msg2="Unable to Deny the User, Please refresh the page";
        this.showMsg();
      }
    );

  }

  clearSearchProd(val){
    // if(val=="name"){
    //   this.searchProdSeller="";
    //   this.searchProdCategory="";  
    // }
    // else if(val=="category"){
    //   this.searchProdSeller="";
    //   this.searchProdName="";  
    // }
    
    // else if(val=="seller"){
    //   this.searchProdName="";
    //   this.searchProdCategory="";  
    // }
  }
//   tab3;onclick = function(){
// this.style.backgroundColor= "#0086d3";
  // }
  openDashSub(val:string){
    if (val=="salesPerformance"){
      this.hideSubtab=false;
      this.btn1color="#0086d3";
      this.btn2color="#f1f1f1";
      this.txt1clr="#f2f2f2";
      this.txt2clr="black";
    }
    else{
      this.hideSubtab=true;
      this.btn2color="#0086d3";
      this.btn1color="#f1f1f1";
      this.txt2clr="#f2f2f2";
      this.txt1clr="black";
    }
  }

  constructor(public service2: cartService, private router: Router, public service: RegistrationService, public serviceAddProduct: AddproductService,public httpService: HttpClient) { }

  checkLogin(){
    let loginUserRole=window.localStorage.getItem("userRole");
    console.log(loginUserRole);
    if(loginUserRole==null)
    {
      console.log("Please login");
      // this.msg2="Please Login";
      // this.showMsgBox1("Login");
      this.router.navigate(["/login"]);
    }
    else if(loginUserRole!="3")
    {
      console.log("You are not Authorised To view this page");
      // this.msg2="You are not Authorised To view this page";
      // this.showMsgBox1("Go Back");
      this.router.navigate(["/homepage"]);
    }
  }

  ngOnInit(){
    this.checkLogin();
    this.openTab("Dashboard");
    
    // this.screenHeight=window.innerHeight+'px';--------------------------------------------------------------
  }
    // this.serviceAddProduct.getAllProduct().subscribe((result: Product[]) => {
     
    
    // console.log("for chart", result);
    // let allLabel={"Electronics":0,"Books":0,"Fashion":0,"Furniture":0,"Phone":0};
    // result.forEach(x => {  
    //   // this.Label.push(x.category.categoryName);  
    //   // this.Data.push(x.productPrice);  
    //   allLabel[x.categoryName]=allLabel[x.categoryName]+1;
    // });  
    // Object.keys(allLabel).forEach(x=>this.Label.push(<string>x));
    // Object.values(allLabel).forEach(x=>this.Data.push(x.toString()));

  chartFunc(){
    this.Data=[];
    this.Label=[];
    this.chartLabels.forEach(x=>this.Label.push(x.toString()));
    this.chartValues.forEach(y=>this.Data.push(y.toString()));
    // this.Data.push(Object.values(allLabel));
    // console.log(this.Data);
    // this.Label.push(<string[]>Object.keys(allLabel));
    // console.log(this.Label);

    var unique = this.Label.filter(function(elem, index, self){

      return index === self.indexOf(elem);

    })
    
    this.Linechart = new Chart('canvas', {  
      type: 'line',  
      data: {  
        labels: this.Label,  

        datasets: [  
          {  
            data: this.chartPrice,  
            borderColor: '#3cb371',  
            backgroundColor: "#FFFFFF",  
            
          }  
        ]  
      },  
      options: {  
        legend: {  
          display: false  
        },  
        scales: {  
          xAxes: [{  
            display: true,
            labelString:'Categories'
          }],  
          yAxes: [{  
            display: true,
            labelString:'Sale Amount'  
          }],  
        }  
      }  
    });  

    this.chart = new Chart('canvas1', {  
      type: 'pie',  
      data: {  
        labels: this.Label,  
        datasets: [  
          {  
            data: this.Data,  
            borderColor: "white",  
            backgroundColor: [  
              "#ff6b61",  
              "#ffd061",  
              "#a1e866",  
              "#65e6b6",  
              "#65cae6",
              "#657ae6",  
              "#561e73",  
              "#e665e0",  
              "#e6658e",
              "#fc3232",
              "#007e91",
              "#178a77",  
              "Blue",  
              "Red",  
              "Blue"  
            ],  
            fill: true  
          }  
        ]  
      },  
      options: {  
        legend: {  
          display: true  
        },  
        // scales: {  
        //   xAxes: [{  
        //     display: true  
        //   }],  
        //   yAxes: [{  
        //     display: false  
        //   }],  
        // }  
      }  
    });  
  // }); 
  
  }

  chartLabels:string[]=[];
  chartValues:number[]=[];
  chartPrice:number[]=[];
  chartProdIds=[];
  ///////////////////////////////

  openTab(tabName) {
    console.log(tabName);
    this.page=1;

    this.hideAlltabs();
    
    if(tabName=="Dashboard"){
      // call the service and get dashboard data and write the below code in success part
      this.hideAlltabs();
      this.dashclr="#2874f0";
      this.clientclr="#fff";
      this.prodclr="#fff";
      this.orderclr="#fff";
      this.catclr="#fff";
      this.dashtxtclr="#fff";
      this.serviceAddProduct.getAllCategories().subscribe(
        data=>{
          this.chartLabels=[];
          this.chartValues=[];
          this.chartPrice=[];
          for(let ii=0;ii<data.length;ii++){
            this.chartLabels.push(data[ii].categoryName);
            this.chartValues.push(0);
            this.chartPrice.push(0);
          }
          this.service2.getAllOrders().subscribe(
            data2 =>{
              // console.log(data2);
              if(data2!=null && data2.length!=0){
                let prodIdSet = new Set();
                this.chartProdIds=[];
                for(let jj=0;jj<data2.length;jj++){
                  prodIdSet.add(data2[jj].productId);
                }
                this.chartProdIds=Array.from(prodIdSet);
                console.log(this.chartProdIds);
                this.service2.getAllProducts(this.chartProdIds).subscribe(
                  prods=>{
                    prods=<Product[]>prods;
                    for(let kk=0;kk<this.chartLabels.length;kk++){
                      let catName = this.chartLabels[kk];
                      for(let ll=0;ll<prods.length;ll++){
                        if(prods[ll].categoryName==catName){
                          for(let mm=0;mm<data2.length;mm++){
                            if(data2[mm].productId==prods[ll].productId){
                              this.chartValues[kk]+=data2[mm].quantity;
                              this.chartPrice[kk]+=data2[mm].totalAmount;
                            }
                          }
                        }
                      }
                    }
                    this.chartFunc();
                  },
                  error=>console.log("chart product Error", error)
                )
              }
            },
            error=>{
              console.log("Error Occured !!", error);
            }
          );
        },
        error=>{
          console.log("error while fetching all Categories",error)
        }
      )
      this.hideLoader=true; 
      this.hideDashBoard=false;
    }

    else if(tabName=="Client"){
      // call the service and get client data and write the below code in success part
      
      console.log("going to client");
      this.dashclr="#fff";
      this.clientclr="#2874f0";
      this.prodclr="#fff";
      this.orderclr="#fff";
      this.catclr="#fff";
      this.clienttxtclr="#fff";
      this.service.getAllUser().subscribe(
        data =>{
          console.log(data);
          this.totalRecords=data.length;
          if(data!=null && data.length!=0){
            this.users=[];
            this.users= <User[]> data;
            for(let i=0;i<this.users.length;i++){
              if(this.users[i].kycStatus==null){
                this.users[i].kycStatus="";
              }
            }
            this.hideLoader=true;
            this.hideClientList=false;
          }
          // console.log(this.products);
        },
        error=>{
          console.log("Error Occured !!", error);
        }        
      );
    }
    else if(tabName=="Product"){
      console.log("going to products");
      this.dashclr="#fff";
      this.clientclr="#fff";
      this.prodclr="#2874f0";
      this.orderclr="#fff";
      this.catclr="#fff";
      this.prodtxtclr="#fff";
      this.serviceAddProduct.getAllProduct().subscribe(
        data =>{
          console.log(data);
          this.totalRecords=data.length;
          if(data!=null && data.length!=0){
            this.products=[];
            // let data2=data;
            this.products= data;
            for(let i=0;i<this.products.length;i++){
              if(this.products[i].createdBy==null){
                this.products[i].createdBy="";
              }
              if(this.products[i].productStatus==null){
                this.products[i].productStatus="";
              }
              if(data[i].sellerName!=null)
                this.products[i].createdBy=data[i].sellerName;
              else{
                this.products[i].createdBy="NA";
              }
            }
            this.hideLoader=true;
            this.hideProductList=false;
          }
          // console.log(this.products);
        },
        error=>{
          console.log("Error Occured !!", error);
        }
      );
    }

    else if(tabName=="Order"){
      // call the service and get order data and write the below code in success part
      console.log("going to Orders");
      this.dashclr="#fff";
      this.clientclr="#fff";
      this.prodclr="#fff";
      this.orderclr="#2874f0";
      this.catclr="#fff";
      this.ordertxtclr="#fff";
      this.service2.getAllOrders().subscribe(
        data =>{
          console.log(data);
          this.totalRecords=data.length;
          if(data!=null && data.length!=0){
            this.orders=[];
            this.orders= <Order[]> data;
            this.hideLoader=true;
            this.hideOrderList=false;
          }
          // console.log(this.products);
        },
        error=>{
          console.log("Error Occured !!", error);
        }
      );
      this.hideLoader=true;
      this.hideOrderList=false;
    }
    else if(tabName=="Category"){
      this.hideAlltabs();
      this.dashclr="#fff";
      this.clientclr="#fff";
      this.prodclr="#fff";
      this.orderclr="#fff";
      this.catclr="#2874f0";
      this.cattxtclr="#fff";
      this.serviceAddProduct.getAllCategories().subscribe(
        data=>{
          console.log(data);
          this.allCategories=data;
          this.hideLoader=true;
          this.hideCategory=false;
        },
        error=>{
          console.log("error while fetching all Categories",error)
        }
      );
      this.serviceAddProduct.getAllBrands().subscribe(
        dataBrand=>{
          console.log(dataBrand);
          this.allBrands=dataBrand;
          this.hideLoader=true;
          this.hideCategory=false;
        },
        error=>{
          console.log("error while fetching all Brands",error)
        }
      )
    }
  }

  showMsg(){
    this.msgDiag.myfunc();
  }
  showMsgBox1(bName?){
    this.msgDiag.showbox1(bName);
  }
  showMsgBox2(b1Name?,b2Name?){
    this.msgDiag.showbox2(b1Name,b2Name);
  }
  msgResult(val){
  }

}
