import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoginSuccessComponent } from './login-success/login-success.component';
import { NgxCaptchaModule } from 'ngx-captcha';
import { ForgotpwdComponent } from './forgotpwd/forgotpwd.component';
import { UserdetailsComponent } from './userdetails/userdetails.component';
import { UserprofileComponent } from './userprofile/userprofile.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { OTPComponent } from './otp/otp.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { AddproductService } from './shared/addproduct.service';
import { HomepageComponent } from './homepage/homepage.component';
import { HomepageService } from './shared/homepage.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { AdminApprovalComponent } from './admin-approval/admin-approval.component';
import { SearchfilterPipe } from './searchfilter.pipe';
import { SearchfilteruserPipe } from './searchfilteruser.pipe';
import { ProductdetailsComponent } from './productdetails/productdetails.component';
import { FusionChartsModule } from "angular-fusioncharts";
import { ChartsModule } from 'ng2-charts';
import { RatePrroductComponent } from './rate-prroduct/rate-prroduct.component';
import { PlaceorderComponent } from './placeorder/placeorder.component';
import { CartComponent } from './cart/cart.component';
import { AvgRatingComponent } from './avg-rating/avg-rating.component';
import { cartService } from './shared/cart.service';
import { RegistrationService } from './registration.service';
import { SearchprodPipe } from './searchprod.pipe';
import { MsgDialogueComponent } from './msg-dialogue/msg-dialogue.component';
import { UserprofileAdminComponent } from './userprofile-admin/userprofile-admin.component';
import { UserprofileVendorComponent } from './userprofile-vendor/userprofile-vendor.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { FilterProductComponent } from './filter-product/filter-product.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { MyratingsComponent } from './myratings/myratings.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';



@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    LoginComponent,
    RegisterComponent,
    LoginSuccessComponent,
    ForgotpwdComponent,
    UserdetailsComponent,
    UserprofileComponent,
    FooterComponent,
    NavbarComponent,
    OTPComponent,
    AddproductComponent,
    HomepageComponent,
    AdminApprovalComponent,
    SearchfilterPipe,
    SearchfilteruserPipe,
    ProductdetailsComponent,
    RatePrroductComponent,
    PlaceorderComponent,
    CartComponent,
    AvgRatingComponent,
    SearchprodPipe,
    MsgDialogueComponent,
    UserprofileAdminComponent,
    UserprofileVendorComponent,
    OrderDetailsComponent,
    FilterProductComponent,
    WishlistComponent,
    MyratingsComponent,
  ],
  imports: [
    ChartsModule,
    FusionChartsModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgxCaptchaModule, 
    NgxPaginationModule,
  ],
  providers: [
    AddproductService,
    HomepageService,
    cartService,
    RegistrationService,
    {provide: LocationStrategy, useClass: HashLocationStrategy},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
