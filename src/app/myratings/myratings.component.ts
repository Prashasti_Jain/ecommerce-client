import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MsgDialogueComponent } from '../msg-dialogue/msg-dialogue.component';
import { RegistrationService } from '../registration.service';
import { Review } from '../review';
import { AddproductService } from '../shared/addproduct.service';
import { Awsurl } from '../shared/awsurl';
import { cartService } from '../shared/cart.service';
import { Image } from '../shared/Image';
import { Product } from '../shared/product';
import { Wishlist } from '../wishlist';
@Component({
  selector: 'app-myratings',
  templateUrl: './myratings.component.html',
  styleUrls: ['./myratings.component.css']
})
export class MyratingsComponent implements OnInit {

  @ViewChild(MsgDialogueComponent) msgDiag:MsgDialogueComponent;

  @Output() editReview = new EventEmitter<any>();
  @Output() diag = new EventEmitter<any>();
  msg="";
  userRole;
  cartCount=0;
  loader1=true;
  // wish:Wishlist[]=[];
  hideRevArr:boolean[]=[];
  allReviews:Review[]=[];
  products:Product[]=[];
  prodIds=[];
  awsUrl = Awsurl.awsUrl;
  hideOrders=true;
  page=1;
  removedProd:Product;
  removedProdNum:number;
  constructor(public serviceProd: AddproductService, public serviceCart: cartService,public serviceReg: RegistrationService) { }
  // getAllWishlist(){
  //   this.serviceProd.getMyWishlist().subscribe(
  //     data=>{
  //       this.page=1;
  //       this.wish=data;
  //       this.prodIds=[];
  //       // let prodIdSet = new Set();
  //       for(let i=0;i<this.wish.length;i++){
  //         // prodIdSet.add(this.orders[i].productId);
  //         this.prodIds.push(this.wish[i].productId);
  //       }
  //       // this.prodIds = Array.from(prodIdSet);
  //       this.serviceCart.getAllProducts(this.prodIds).subscribe(
  //         data2=>{
  //           this.products=data2;
  //           for(let j=0;j<this.products.length;j++){
  //             console.log(this.products[j].productName);
  //             this.products[j].createdBy=data2[j].sellerName;
  //             if(this.products[j].image.length==0 || this.products[j].image==null){
  //               let img = new Image();
  //               img.imageId=null;
  //               img.imageURL="../../assets/no-image.jpg";
  //               this.products[j].image.push(img);
  //             }
  //             else{
  //               this.products[j].image[0].imageURL=this.awsUrl+this.products[j].image[0].imageURL;
  //             }
  //           }
  //           console.log(data2);
  //         },
  //         error=> console.log("could not fetch product",error)
  //       )
  //     },
  //     error=>console.log("error while fetching wishlist",error)
  //   )
  // }

  getMyRatings(){
    this.serviceProd.getAllMyReviews().subscribe(
      dataRev=>{
        this.prodIds=[];
        this.hideRevArr=[];
        console.log(dataRev);
        this.allReviews=dataRev;
        // console.log("sdfhk--------------------------------------------------")
        for(let i=0;i<this.allReviews.length;i++)
        {
          this.allReviews[i].productId=dataRev[i].product.productId;
          this.prodIds.push(this.allReviews[i].productId);
          this.hideRevArr.push(true);
        }
        this.serviceCart.getAllProducts(this.prodIds).subscribe(
          data2=>{
            this.products=data2;
            for(let j=0;j<this.products.length;j++){
              console.log(this.products[j].productName);
              this.products[j].createdBy=data2[j].sellerName;
              if(this.products[j].image.length==0 || this.products[j].image==null){
                let img = new Image();
                img.imageId=null;
                img.imageURL="../../assets/no-image.jpg";
                this.products[j].image.push(img);
              }
              else{
                this.products[j].image[0].imageURL=this.awsUrl+this.products[j].image[0].imageURL;
              }
            }
            console.log("aaa",data2);
          },
          error=> console.log("could not fetch product",error)
        )
      }
    )
  }
  rateProduct(id,name){
    this.editReview.emit({"id":id,"name":name});
  }

  deleteReview(rev:Review){
    this.serviceProd.deleteMyReview(rev.productId).subscribe(
      data=>{
        console.log("Deleted Sucessfully");
        this.getMyRatings();
      },
      error=>console.log("error deleting review", error)
    )
  }


  // removeFromWishlist(prod:Product,i){
    // this.removedProd=prod;
    // this.removedProdNum=i;
    // // console.log(id);
    // this.msg="Are you sure you want to remove this product from your Wishlist?";
    // this.showMsgBox2("Delete","Cancel");
    // this.removedProd=prod;
    // this.removedProdNum=i;
    // this.removeProd.emit(prod.productId);
  // }

  // confirmRemove(val:number){
  //   let prodId = this.removedProd.productId;
  //   if(val==1){
  //     this.serviceProd.deleteFromWishlist(prodId).subscribe(
  //       data=>{
  //         console.log(data);
  //         let val: string = data.value;
  //         if(val.substring(0,15)=="product removed"){
  //           this.msg="Product Removed Successfully";
  //           this.showMsgInParent();
  //           this.products.splice(this.removedProdNum,1);
  //           this.wish.splice(this.removedProdNum,1);
  //           this.removedProd=null;
  //           this.removedProdNum=null;
  //         }
  //         else{
  //           this.msg="Some error occured. Please Refresh the page...";
  //           this.showMsgInParent();
  //         }
  //       },
  //       error => {
  //         console.log("Error !!!!! ", error)
  //         this.msg="Some error occured. Please Refresh the page";
  //         this.showMsgInParent();
  //       }
  //     )
  //   }
  //   else{
  //     console.log("Item is not removed from wishlist");
  //   }
  // }

  showMsgInParent(){
    this.diag.emit(this.msg);
  }

  ngOnInit(): void {
    // this.checkRole();    
    this.loader1=true;
    this.hideOrders=true;
    // this.getAllWishlist();
    this.getMyRatings();
  }

  // hideAllReview(){
  //   for(let i=0;i<this.hideRevArr.length;i++){
  //     this.hideRevArr[i]=true;
  //   }
  // }

  // showReview(i){
  //   this.hideAllReview();
  //   this.hideRevArr[i]=false;
  // }

  // toggleRev(i){
  //   if(this.hideRevArr[i]==false){
  //     this.hideAllReview();
  //   }
  //   else{
  //     this.showReview(i);
  //   }
  // }

}
