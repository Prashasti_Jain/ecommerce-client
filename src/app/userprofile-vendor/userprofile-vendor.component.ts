import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegistrationService } from '../registration.service';
import { User } from '../user';

@Component({
  selector: 'app-userprofile-vendor',
  templateUrl: './userprofile-vendor.component.html',
  styleUrls: ['./userprofile-vendor.component.css']
})
export class UserprofileVendorComponent implements OnInit {

  hideSubMenu=true;
  user:User;
  isShow=false;
  isAddressDetailsShow=true;
  msg = '';
  varfname:string;
  notEditUser=true;
  notEditAddress=true;


  showSubmenu(){
    this.hideSubMenu=!this.hideSubMenu;
  }
  editUserDetails(){
    this.notEditUser=false;

  }
  editAddressDetails(){
    this.notEditAddress=false;
  }

  noteditUserDetails(){
    this.notEditUser=true;
    this._service.updateUserDetails(this.user).subscribe(
      data=>console.log("User Details updated"),
      error=>console.log("unable to update User Details", error)
    );
    //update userzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
  }
  noteditAddressDetails(){
    this.notEditAddress;
    this._service.updateAddress(this.user.address).subscribe(
      data=>console.log("Address updated"),
      error=>console.log("unable to update address ", error)
    );
  }
  constructor(private _service:RegistrationService ,private _router:Router) { }

  ngOnInit(): void {
    this.user =new User();
    this.showPersonalDetails();
    // this.fetchAddress();
    //aesdjuyhsadbjgbhsdajhfsadbvjubgfdgsdabbbbbazzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
  }

  goToCart(){
    
  }
  toggle(){

  }


  fetchAddress(userdata){
    this._service.getAddress().subscribe(
      data=>{
        this.user.address=data;
        this.user.id=userdata.id;
        this.user.firstName=userdata.firstName;
        this.user.lastName=userdata.lastName;
        this.user.middleName=userdata.middleName;
        this.user.gender=userdata.gender;
        this.user.primaryContact=userdata.primaryContact;
        this.user.email=userdata.email;
        this.user.alternateContact=userdata.alternateContact;
      },
      error=> console.log("Error while fetching address",error)
    )
  }

  toggleDisplay(){
    this.isShow=false;
  }
  toggleAddressDisplay()
  {
    this.isAddressDetailsShow=false;
  }

  showPersonalDetails(){
    this._service.getUserByEmail().subscribe(
      
      data => {​​
      console.log("response received");
      // this._router.navigate(['/loginSuccess']);
      sessionStorage.setItem("login ","success");
      this.fetchAddress(data);
      // this.user=<User>data;
      }​​,
      error =>{​​
      console.log("exception occured");
      this.msg = "Incorrect Credentials,Please enter correct username and password";
      }​​
      
      );
  }
 
}