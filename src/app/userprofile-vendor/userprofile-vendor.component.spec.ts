import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserprofileVendorComponent } from './userprofile-vendor.component';

describe('UserprofileVendorComponent', () => {
  let component: UserprofileVendorComponent;
  let fixture: ComponentFixture<UserprofileVendorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserprofileVendorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserprofileVendorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
