import { Address } from './address';

    export class User {
        id :number;
        email:string;
        firstName:string;
        middleName:string;
        lastName: string;
        primaryContact : number;
        alternateContact:number;
        password:string;
        cpassword:string;
        gender:string;
        securityAnswer: string;
        radiobutton:boolean;
        otp:string;
        // password:string;
        optionForgetpwd:boolean;
        securityQuestion:string;
        iAgree:boolean;
        roleId : number;
        kycStatus : string;
        address: Address;
        constructor(){
            this.address = new Address();
        }
    }
