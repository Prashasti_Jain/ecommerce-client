import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HomepageService } from '../shared/homepage.service';
// import { Product } from '../shared/product.model';
import { Product } from '../shared/product';
import { Awsurl } from '../shared/awsurl'
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; 
import { getAttrsForDirectiveMatching } from '@angular/compiler/src/render3/view/util';
import { Image } from '../shared/Image';
import { Cartmodel } from '../cartmodel';
import { AddproductService } from '../shared/addproduct.service';
import { cartService } from '../shared/cart.service';
import { MsgDialogueComponent } from '../msg-dialogue/msg-dialogue.component';
import { FilterProductComponent } from '../filter-product/filter-product.component';
import { AUTO_STYLE } from '@angular/animations';
@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  @ViewChild(MsgDialogueComponent) msgDiag:MsgDialogueComponent;
  @ViewChild(FilterProductComponent) filter:FilterProductComponent;
  msg="";
  search=new Product();
  products: Array<Product>;
  productsReceived: Array<Product>;
  allProducts:Product[];
  searchId1;
  allSearcheddata : Product []=[];
  imgSrc;
  arrimg: string[]=[];

  totalRecords;
  totalRecord: Number;
  page: Number=1;
  cartCount=0;
  userRole;
  itemsPerPage=9;
  homepage:string="homepage";
  
  constructor(private router: Router, public service: HomepageService, public cartService: cartService, private service2: AddproductService, private route: ActivatedRoute) { }


  searchProduct2(data){
    this.searchId1=data;
    console.log(data);
  }
  ngOnInit() {
    this.checkLogin();
    this.checkRole();
    this.service.getAll().subscribe(
      response => this.handleSuccessfulResponse(response),
    );
    this.searchId1 = this.route.snapshot.paramMap.get('prodName');

  }
  checkLogin(){
    let loginUserRole=window.localStorage.getItem("userRole");
    console.log(loginUserRole);
    if(loginUserRole==null)
    {
      console.log("Please login");
      this.router.navigate(["/login"]);
    }
  }
  
  
  handleSuccessfulResponse(response) {
    this.products = new Array<Product>();
    this.productsReceived = response;
    console.log("abc",this.productsReceived)
    for (const prod of this.productsReceived) {
      const productretrieved = new Product();
      productretrieved.productName = prod.productName;
      productretrieved.productPrice = prod.productPrice;
      productretrieved.productDesc = prod.productDesc;      
      productretrieved.categoryName = prod.categoryName;
      productretrieved.categoryId = prod.categoryId;
      productretrieved.productId = prod.productId;
      productretrieved.productQuantity = prod.productQuantity;
      productretrieved.brandName = prod.brandName;
      productretrieved.color = prod.color;  
      
      //this.img=prod.imageId.split(";");
      //productretrieved.imageId = this.img[0];      
      //this.imgSrc=this.img[0];
      let im = new Image();
      if(prod.image.length!=0){
        this.arrimg=[];
        for(let i=0;i<prod.image.length;i++){
          this.arrimg.push(prod.image[i].imageURL);
        }
        if(this.arrimg.length!=0)
          this.imgSrc= Awsurl.awsUrl+this.arrimg[0];
        //window.alert(this.arrimg);
        //window.alert(this.imgSrc);
        console.log(this.imgSrc);
      }
      else
        this.imgSrc="../../assets/no-image.jpg";
      im.imageURL=this.imgSrc;
      // console.log(productretrieved);
      productretrieved.image.push(im);
      // console.log("new--- ",productretrieved);
      //window.alert(productretrieved.imageId);
      
      this.products.push(productretrieved);
      // this.allProducts.push(productretrieved);
      this.totalRecords=this.products.length;
      //window.alert(this.totalRecords);
    }
    this.allProducts=this.products;
    this.filter.populate(this.allProducts);
  } 
  
  filterProdDisplay(event){
    console.log("eventvalues",event);
    this.products=event;
    this.totalRecords=this.products.length;
    this.priceLH=0;
  }

  priceLH=0;
  sortFunction(val:string){
    if(val=="priceLowToHigh"){
      this.priceLH=1;
      this.products.sort(function(a,b){return a.productPrice-b.productPrice}) 
    }
    if(val=="priceHighToLow"){
      this.priceLH=-1;
      this.products.sort(function(a,b){return b.productPrice-a.productPrice}) 
    }
  }




  isShow=false;
  isSearch=true;
  isSearch1=true;
  btnshow=false;
  noProd=true;

  toggle(){
    
    this.isShow=false;
    this.isSearch=true;
  }

  toggleDispSearch(pName){
    this.isShow=false;
    this.isSearch=true;
    this.searchProduct(pName);
  }

  searchProduct(pName){
    if(pName==""){
      // window.alert();
      this.msg="Enter a Product Name to search";
      this.showMsg();
    }
    else{
      this.isShow=true;
      this.isSearch=false;
      this.page=1;
      //this.isSearch1=false;
      this.service.sendSearch(pName).subscribe(
        data => {console.log("Success !!! ", data)
          var data1:Product;
          if (data != null && data.length!=0){
            this.allSearcheddata=[];
            //window.alert(data.length);
            this.totalRecord=data.length;
            for (var i=0;i<data.length;i++){
              data1 = <Product> data[i];
              this.allSearcheddata[i] = new Product();
              this.allSearcheddata[i].productName=data1.productName;
              this.allSearcheddata[i].productPrice=data1.productPrice;
              this.allSearcheddata[i].productId=data1.productId;
              this.allSearcheddata[i].productQuantity=data1.productQuantity;
              
              if(data1.image!=null || data1.image.length!=0){
                this.arrimg=[];
                for(let i=0;i<data1.image.length;i++){
                  this.arrimg.push(data1.image[i].imageURL);
                }
                
                if(this.arrimg.length!=0)
                  this.imgSrc= Awsurl.awsUrl+this.arrimg[0];
              }
              else
                this.imgSrc="../../assets/no-image.jpg";
              this.allSearcheddata[i].image=[];  
              this.allSearcheddata[i].image.push({"imageId":0,"imageURL":this.imgSrc});
              if(data1.category==null){
                  this.allSearcheddata[i].category.categoryName="NA";
                  this.allSearcheddata[i].categoryId=null;
                }
              else{
                this.allSearcheddata[i].category.categoryName=data1.category.categoryName;
              }
              if(data1.productDesc==null){
                this.allSearcheddata[i].productDesc="NA";
                }
              else{
                this.allSearcheddata[i].productDesc=data1.productDesc;
              }
            } 
          }
          else{
            this.resetSearched();
            window.alert("Product not found");
            this.msg="Product not found";
            this.showMsg();
            //this.noProd=false;
            //this.isShow=false;
            window.location.reload();
          }
        },
        error => console.log("Error !!!!! ", error)
      ) ;;
    }
  }
  resetSearched(){
    this.allSearcheddata=null;
  }
  goToCart() {
    this.router.navigate(['/cart']);
  }

  goToProduct(prod){
    this.router.navigate(['/product', prod.productId]);
  }

  ZeroQty(prod:Product){
    if(prod.productQuantity==0)
      return true;
    else
      return false;
  }

  addToCart(product2:Product){
    let productId = product2.productId;
    console.log(productId);
    let cartItem = new Cartmodel();
    cartItem.productId=product2.productId;
    cartItem.quantity=1;
    cartItem.totalPrice=product2.productPrice;
    this.service2.addProdToCart(cartItem).subscribe(
      data=>{
        if (data.value=="Added"){
          // window.alert("item added to cart");
          this.msg="item added to cart";
          this.showMsg();
          this.cartCount+=1;
        }
        else{         
          // window.alert("Sorry! We don't have any more quantity of this item");
          this.msg="Sorry! We don't have any more quantity of this item"
          this.showMsg();
        }
      },
      error=>{
        // window.alert("could not add item to cart");
        this.msg="could not add item to cart";
        this.showMsg();
      }
    );
  }
  
  checkRole(){
    this.userRole=window.localStorage.getItem("userRole");
  }

  showMsg(){
    this.msgDiag.myfunc();
  }
}