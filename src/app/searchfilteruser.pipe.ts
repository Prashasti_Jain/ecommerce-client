import { Pipe, PipeTransform } from '@angular/core';
import { User } from './user';

@Pipe({
  name: 'searchfilteruser'
})
export class SearchfilteruserPipe implements PipeTransform {

  transform(users: User[], searchProd: string[]): User[] {
    console.log(searchProd);
    if(!users || !searchProd){
      return users;
    }
    else{
      return users.filter(
        user => 
        user.firstName.toLocaleLowerCase().includes(searchProd[0].toLocaleLowerCase()) &&
        user.primaryContact.toString().includes(searchProd[1].toLocaleLowerCase()) &&
        user.email.toLocaleLowerCase().includes(searchProd[2].toLocaleLowerCase()) &&
        user.kycStatus.toLocaleLowerCase().includes(searchProd[3].toLocaleLowerCase())
      );
    }
  }

}
