import { Component, Input, OnInit } from '@angular/core';
import { AddproductService } from '../shared/addproduct.service';

@Component({
  selector: 'app-avg-rating',
  templateUrl: './avg-rating.component.html',
  styleUrls: ['./avg-rating.component.css']
})
export class AvgRatingComponent implements OnInit {

  constructor(private service : AddproductService) { }

  @Input() prodId:number;
  @Input() showOnlyNum:boolean;
  
  rating:number;
  count:number;
  
  getRating(){
    // console.log(this.prodId)
    this.service.getAvgRating(this.prodId).subscribe(
      data=>{
        this.rating=data.Value;
        this.count=data.count;
        // console.log(this.rating);
      },
      error=>console.log(error)
    );
  }

  ngOnInit(): void {
    this.getRating();
  }

}
