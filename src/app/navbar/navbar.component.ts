import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { cartService } from '../shared/cart.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Input() parentPage:String=null;
  @Output() searchRes = new EventEmitter<any>();
  cartCount=0
  constructor(private _router: Router, private cartService: cartService) { }
  userRole;
  userFName="User";
  showSub;
  searchName;

  searchProd(){
    if(this.parentPage!=null){
      this.searchRes.emit(this.searchName);
    }
    if(this.searchName!=null && this.searchName!=""){
      console.log(this.searchName);
      console.log(this.parentPage);
      if(this.parentPage==null){
        this._router.navigate(['/homepage', this.searchName]);
      }
      
    }
  }

  ngOnInit(): void {
    this.get_Name_Role();
    this.showSub=false;
    this.cartService.getAll().subscribe(
      data => {
        this.cartCount=0;
        for(let i=0;i<data.length;i++)
        this.cartCount+=data[i].quantity;
      },
      error=> console.log("could not fetch cart")
    );
  }

  toggleSub(event){
   event.stopPropagation();
    this.showSub=!this.showSub;
  }

  hideSubmenu(){
   this.showSub=false;
  }


  goToCart(){
    this._router.navigate(["/cart"]);
  }
  goToMyWishlist(){
    this.showSub=false;
    this._router.navigate(['/userprofile',"wishlist"]);
  }

  goToHome(){
    this._router.navigate(["/homepage"]);
  }

  goToMyProfile(){
    this.showSub=false;
    this._router.navigate(["/userprofile"]);
  }

  goToMyOrders(){
    this.showSub=false;
    this._router.navigate(["/orderdetails"]);
  }

  logout(){    
    window.localStorage.removeItem('EcomToken');
    window.localStorage.removeItem('userRole');
    this._router.navigate(["/login"]);
  }

  goToAddProduct(){
    this.showSub=false;
    this._router.navigate(['/addProduct'])
  }

  goToMyProducts(){
    this.showSub=false;
    this._router.navigate(['/addProduct',"myProducts"])
  }

  goToAdminDashboard(){
    this.showSub=false;
    this._router.navigate(["/admin"]);
  }

  get_Name_Role(){
    this.userRole=window.localStorage.getItem("userRole");
    this.userFName=window.localStorage.getItem("userFName");
  }

}
