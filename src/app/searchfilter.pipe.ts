import { Pipe, PipeTransform } from '@angular/core';
import { Product } from './shared/product';

@Pipe({
  name: 'searchfilter'
})
export class SearchfilterPipe implements PipeTransform {

  transform(products: Product[], searchProd: string[]): Product[] {
    console.log(searchProd);
    if(!products || !searchProd){
      return products;
    }
    else{
      return products.filter(
        product => 
        product.productName.toLocaleLowerCase().includes(searchProd[0].toLocaleLowerCase()) &&
        product.categoryName.toLocaleLowerCase().includes(searchProd[1].toLocaleLowerCase()) &&
        product.createdBy.toLocaleLowerCase().includes(searchProd[2].toLocaleLowerCase()) &&
        product.productStatus.toLocaleLowerCase().includes(searchProd[3].toLocaleLowerCase())
      );
    }
    // else if((searchProd[0]=="" || searchProd[0]==null) && (searchProd[1]=="" || searchProd[1]==null) && (searchProd[2]=="" || searchProd[2]==null)){
    //   return products;
    // }
    // else if(searchProd[0]!=null && searchProd[0]!=""){
    //   return products.filter(
    //     product => product.productName.toLocaleLowerCase().includes(searchProd[0].toLocaleLowerCase())
    //   );
    // }
    // else if(searchProd[1]!=null && searchProd[1]!=""){
    //   return products.filter(
    //     product => product.category.categoryName.toLocaleLowerCase().includes(searchProd[1].toLocaleLowerCase())
    //   );
    // }
    // else if(searchProd[2]!=null && searchProd[2]!=""){
    //   return products.filter(
    //     product => product.createdBy.toLocaleLowerCase().includes(searchProd[2].toLocaleLowerCase())
    //   );
    // }
  }

}
