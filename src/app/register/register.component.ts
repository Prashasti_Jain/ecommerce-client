import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../user';
import { Router } from '@angular/router';
import {RegistrationService } from '../registration.service';
import { MsgDialogueComponent } from '../msg-dialogue/msg-dialogue.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  @ViewChild(MsgDialogueComponent) msgDiag:MsgDialogueComponent;
  user =new User();
  msg = '';
  selectedQue : "";
  msg2 = "";
  
  //constructor(private _service:RegistrationService ,private _router:Router) { }
   
  siteKey:string;
  size:string;
    constructor(private _service:RegistrationService ,private _router:Router) {
    this.siteKey = '6Lc6wVMaAAAAAB0okzLeyZg7Wzj7oJuer1byqKHU';
    this.size='normal';
} 
   
  
  ngOnInit(): void {
  }

  registerUser(Registerform:NgForm)
  {
    // alert(".");
    
    // this._router.navigate(['/loginform']);

    this._service.registerUserFromRemote(this.user).subscribe(
      data => { 
                console.log("response received");
                this.msg2="Account Created! Waiting for approval from the admin";
                this.showMsgBox1("Ok")
                
              },
      error =>{ 
                console.log("exception occured",error);
                // console.log("exception occured",error.json());
                // console.log("exception occured",error.error);
                this.msg2= error.error.message;
                this.msg = "error.error";
                this.showMsgBox1("Ok");
              } 
         );
  }
 
  gotologinpage(){
    console.log("Hi Welcome to E-commerce App");
    this._router.navigate(['/loginform']);

  }
  cancelRegister(Registerform:NgForm){
    //alert("Cancelled");
    //Registerform.resetForm();
  }
  selectChangeHandler(event:any){
    this.selectedQue =event.target.value;
  }
  showMsg(){
    this.msgDiag.myfunc();
  }
  showMsgBox1(bName?){
    this.msgDiag.showbox1(bName);
  }
  showMsgBox2(b1Name?,b2Name?){
    this.msgDiag.showbox2(b1Name,b2Name);
  }
  msgResult(){
    this._router.navigate(['/login']);
  }
}
