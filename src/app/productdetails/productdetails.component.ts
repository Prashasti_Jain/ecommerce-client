import { Component, OnInit, ViewChild } from '@angular/core';
import { AddproductService } from '../shared/addproduct.service';
import { Product } from '../shared/product';
import { Awsurl } from '../shared/awsurl'
import { ActivatedRoute, Router } from '@angular/router';
import { Image } from '../shared/Image';
import { Cartmodel } from '../cartmodel';
import { cartService } from '../shared/cart.service';
import { MsgDialogueComponent } from '../msg-dialogue/msg-dialogue.component';
import { Review } from '../review';

@Component({
  selector: 'app-productdetails',
  templateUrl: './productdetails.component.html',
  styleUrls: ['./productdetails.component.css']
})
export class ProductdetailsComponent implements OnInit {

  @ViewChild(MsgDialogueComponent) msgDiag:MsgDialogueComponent;
  hideProd=true;
  product2 = new Product("","","","","");
  showImg = "../../assets/no-image.jpg";
  imgUrl = [];
  // imgUrl = ["../../assets/no-image.jpg"];
  imgNo=[];
  loader1=false;
  currentImg=0;
  hideRatingDiv=true;
  cartCount=0;
  prodNotAvailable=false;
  searchVal:string;
  msg:string;
  hidemsg=true;
  userRole;
  screenHeight;
  allReviews:Review[]=[]
  showingReviews:Review[]=[];
  showReviewNum:number=0;
  showMore=true;
  reviewSort="";
  constructor(public service: AddproductService, public cartService: cartService, private route: ActivatedRoute,private router: Router) { }


  //search product
  searchProd(){
    // console.log(this.searchval);
    
    this.router.navigate(['/homepage', this.searchVal]);
    //add same search function of home page and redirect homepage
  }

  //get Rating
  
  
  //addToCart function
  addToCart(){
    let productId = this.product2.productId;
    console.log(productId);
    let cartItem = new Cartmodel();
    cartItem.productId=this.product2.productId;
    cartItem.quantity=1;
    cartItem.totalPrice=this.product2.productPrice;
    this.service.addProdToCart(cartItem).subscribe(
      data=>{
        if (data.value=="Added"){
          // window.alert("item added to cart");
          this.msg="Item added to cart";
          this.showMsg();
          this.cartCount+=1;
        }
        else{         
          // window.alert("Sorry! We don't have any more quantity of this item");
          this.msg="Sorry! We don't have any more quantity of this item"
          this.showMsg();
        }
      },
      error=>{
        // window.alert("could not add item to cart");
        this.msg="Could not add item to cart";
        this.showMsg();
      }
    );
  }

  //BuyNow
  addToMyWishlist(){
    console.log("adding to wishlist now");
    let productId = this.product2.productId;
    this.service.addToWishlist(productId).subscribe(
      data=>{
        console.log("added");
        this.msg="Item added to Wishlist";
        this.showMsg();
      },
      error=>console.log(error)
    )
  }

  changeImg(val){
    // console.log(val);
    this.showImg=this.imgUrl[val];
    this.currentImg=val;
  }

  nextImg(){
    // console.log(this.imgUrl);
    console.log(this.currentImg);
    if(this.imgUrl!=null && this.imgUrl.length!=0){
      if(this.currentImg>=this.imgUrl.length-1)
      {
        this.currentImg=0;
      }
      else{
        this.currentImg++;
      }
      this.showImg=this.imgUrl[this.currentImg];
    }
  }

  getReviews(id){
    this.service.getAllReviews(id).subscribe(
      dataRev=>{
        this.allReviews=dataRev;
        console.log(dataRev);
        this.showingReviews=[];
        for(let i=0;i<this.allReviews.length;i++){
          if(i>=1)
            break;
          this.showingReviews.push(this.allReviews[i]);
        }
        if(this.showingReviews.length>=this.allReviews.length)
        this.showMore=false;
      },
      error=>console.log(error)
    )
  }

  sortFunction(){
    let val=this.reviewSort;
    console.log(val);
    if(val=="LowToHigh"){
      this.allReviews.sort(function(a,b){return a.rating-b.rating}) 
    }
    if(val=="HighToLow"){
      this.allReviews.sort(function(a,b){return b.rating-a.rating}) 
    }
    if(val=="Newest"){
      this.allReviews.sort(function(a,b){
        let d1 = a.date.substring(0,4)+a.date.substring(5,7)+a.date.substring(8,10);
        let d2 = b.date.substring(0,4)+b.date.substring(5,7)+b.date.substring(8,10);
        let nd1 = Number(d1);
        let nd2 = Number(d2);
        return nd2-nd1;
      }) 
    }
    this.showingReviews=[];
    this.showMoreReview();
  }

  showMoreReview(){
    if(this.showingReviews.length>=this.allReviews.length)
      this.showMore=false;
    else{
      let j=0;
      for(let i=this.showingReviews.length;i<this.allReviews.length;i++){
        if(j>=1)
          break;
        this.showingReviews.push(this.allReviews[i]);
        j=j+1;
      }
      if(this.showingReviews.length>=this.allReviews.length)
        this.showMore=false;
      else{
        this.showMore=true;
      }
    }
  }

  prevImg(){
    
    console.log("prev=",this.currentImg);
    if(this.imgUrl!=null && this.imgUrl.length!=0){
      if(this.currentImg<=0)
      {
        this.currentImg=this.imgUrl.length-1;
      }
      else{
        this.currentImg--;
      }
      this.showImg=this.imgUrl[this.currentImg];
    }
  }
  rateThisProduct(){
    this.hideRatingDiv=false;
  }
  submitRating(val:number){
    this.hideRatingDiv=true;
    //submit rating to server;
  }
 
  //get product details
  getProduct(id){
    this.loader1=false;
    this.service.sendSearchById(id).subscribe(
      data => {
        console.log("Success, Product fetched!-!-!",data)
        // let cName = data.categoryName;
        if(data.productQuantity==0){
          this.prodNotAvailable=true;
        }
        this.product2 = <Product> data;
        // this.product2.category.categoryName=cName;
        if(this.product2.image!=null && this.product2.image.length!=0){
          this.imgUrl=[];
          this.imgNo=[];
          let ss:Image[]=this.product2.image;
          // ss.pop();
          // console.log(ss);
          for(let i=0;i<ss.length;i++){
            let temp = Awsurl.awsUrl+ss[i].imageURL;
            
            // console.log(temp);
            // console.log(this.product2.awsUrl);
            this.imgUrl.push(temp);
            // console.log(this.imgUrl[i]);
            this.imgNo.push(i);
          }
          this.showImg=this.imgUrl[0];
        }
        else{
          this.imgUrl=[];
          this.imgUrl.push("../../assets/no-image.jpg");
        }
        
        // console.log(this.product2);
        this.loader1=true;
        this.hideProd=false;
      },
      error => {
        console.log("error occured !-!-!", error);
        this.loader1=true;
        this.hideProd=false;
      }
    );
  }

  checkLogin(){
    let loginUserRole=window.localStorage.getItem("userRole");
    console.log(loginUserRole);
    if(loginUserRole==null)
    {
      console.log("Please login");
      this.router.navigate(["/login"]);
    }
  }
  

  ngOnInit(): void {
    this.checkLogin();
    this.screenHeight=window.innerHeight+'px';
    // console.log(this.screenHeight)
    this.checkRole();
    let id = this.route.snapshot.paramMap.get('prodId');
    this.product2.productId=id;
    this.getProduct(id);
    this.getReviews(id);
  }


  showMsg(){
    this.msgDiag.myfunc();
  }

  checkRole(){
    this.userRole=window.localStorage.getItem("userRole");
  }

}
