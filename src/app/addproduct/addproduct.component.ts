import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { format } from 'path';
import { $ } from 'protractor';
import { Awsurl } from '../shared/awsurl';
import { AddproductService } from '../shared/addproduct.service';
import { Image } from '../shared/Image';
import { Product } from '../shared/product';
import { Category } from '../shared/Category';
import { MsgDialogueComponent } from '../msg-dialogue/msg-dialogue.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Brand } from '../brand';
import { ProdColors } from '../prodColors';


@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css']
})
export class AddproductComponent implements OnInit {
  @ViewChild(MsgDialogueComponent) msgDiag:MsgDialogueComponent;
  msg2="";
  url;
  msg = "";
  imgArray: any[];
  output;
  output2;
  isShow=true;
  isSearch=true;
  btnshow=true;
  noProd=true;
  showCreatedBy=true;
  noProdCreatedBy=true;
  searchId1;
  allSearcheddata : Product []=[];
  allCreatedBy : Product []=[];
  cnt = 0;
  imgSent=0;
  loader1=true;
  hideUpdateBox=true;
  updatedProduct:Product = new Product();
  updatedProductImage: Image[]=[];
  awsUrl = Awsurl.awsUrl;
  deletedImageIdList=[];
  allCategories: Category[]=[];
  allBrands: Brand[]=[];
  allColors=ProdColors.allColors;
  msgResultValue=0;
  deleteProdNum:Product;
  hideAddProd=false;
  constructor(public service: AddproductService,private route: ActivatedRoute, private router: Router) { }

  deleteImg(id,index){
    this.updatedProduct.image.splice(index,1);
    this.deletedImageIdList.push(id);
  }

  //Delete the selected Product
  deleteProd(ithProd){
    this.deleteProdNum=ithProd;
    this.msg2="Are you sure you want to delete this product from the database?";
    this.showMsgBox2("Delete","Cancel");
  }

  confirmDelete(){
    let ithProd=this.deleteProdNum;
    let ith=ithProd.productId;
    console.log(ith);
    if (this.msgResultValue==1) {
      console.log('Product is getting deleted from the database.');
      this.loader1=false;
      this.service.deleteProduct(ith).subscribe(
        data => {console.log("Success !!! ", data)
          // window.alert("Product Deleted Successfully");
          this.msg2="Product Deleted Successfully";
          this.showMsg();
          // this.showAllProdCreateBy();
          console.log(this.allCreatedBy);
          const index: number = this.allCreatedBy.indexOf(ithProd);
          if (index !== -1) {
              this.allCreatedBy[index].productQuantity=0;
              this.allCreatedBy[index].productStatus="Deleted";
              console.log(this.allCreatedBy);
          }
          this.loader1=true; 
        },
        error => {
          console.log("Error !!!!! ", error)
          this.loader1=true;
        }
      ) ;
    } 
    else {
      console.log('Product is not deleted from the database.');
    }
  }

  //Image is getting stored in the division to view
  storeImg(event){
    var picFile = event.target;
    var div = document.createElement("img");
    div.style.display="inline";
    div.style.margin="2px";
    div.style.paddingLeft="1px";
    div.style.height="60px";
    div.style.width="60px";
    div.style.border="1px";
    div.style.padding="1px";
    div.style.borderRadius="5px";
    div.style.borderColor="Black";
    div.style.borderStyle="Solid";
    div.className='thumbnail';
    div.src = picFile.result as string;
    // this.imgArray.push(picFile.result);
    this.output.insertBefore(div, null);
  }


  //For Selecting images from storage
	selectFile(event,field:string) {
		if((!event.target.files[0] || event.target.files[0].length == 0) && this.cnt==0) {
			this.msg = 'You must select an image';
			return;
    }
    this.cnt=1;
    this.msg="";
    var files = event.target.files; //FileList object
    // console.log(this);
    this.output = document.getElementById(field);
    for (let i = 0; i < files.length; i++) {
      let reader = new FileReader();
      reader.addEventListener("load", this.storeImg.bind(this) );
      let file = files[i];
      if (!file.type.match('image')) {
        this.msg = "Only images are supported";
        return;
      }
      
      this.imgArray.push(file);
      reader.readAsDataURL(file);
    }
    this.msg = "";
  }
  

  //If no image is selected then this goes on
  noImgWarning(){
    if(this.cnt==0){
    this.msg = 'You must select an image';
    }
  }


  //Show Add product page
  toggleDisplay(){
    this.isShow=false;
    this.isSearch=true;
    this.btnshow=false;
    this.noProd=true;
    this.showCreatedBy=true;
    this.noProdCreatedBy=true;
    this.hideUpdateBox=true;
    this.searchId1='';
    this.resetSearched();
    this.resetSearchedCreatedBy();
    this.imgArray=[];
    // this.loader1=true;
  }

  //Show Searched details
  toggleDispSearch(){
    this.isShow=true;
    this.isSearch=false;
    this.btnshow=true;
    this.noProd=true;
    this.showCreatedBy=true;
    this.noProdCreatedBy=true;
    this.hideUpdateBox=true;
    this.resetForm();
    this.resetSearchedCreatedBy();
    // this.loader1=true;
  }

  //Show products created by this user
  toggleDispCreatedBy(){
    this.isShow=true;
    this.isSearch=true;
    this.btnshow=true;
    this.noProd=true;
    this.showCreatedBy=false;
    this.noProdCreatedBy=true;
    this.hideUpdateBox=true;
    this.resetForm();
    this.resetSearched();
    // this.loader1=true;
  }

  //Reset Searched Table
  resetSearched(){
    this.allSearcheddata=null;
    // this.allSearcheddata.push(this.cleared);
  }

  resetSearchedCreatedBy(){
    this.allCreatedBy=null;
    // this.allSearcheddata.push(this.cleared);
  }


  checkLogin(){
    let loginUserRole=window.localStorage.getItem("userRole");
    console.log(loginUserRole);
    if(loginUserRole==null)
    {
      console.log("Please login");
      // this.msg2="Please Login";
      // this.showMsgBox1("Login");
      this.router.navigate(["/login"]);
    }
    else if(loginUserRole!="2")
    {
      console.log("You are not Authorised To view this page");
      // this.msg2="You are not Authorised To view this page";
      // this.showMsgBox1("Go Back");
      this.router.navigate(["/homepage"]);
    }
  }


  // search by product name and update on table
  searchProduct(pName){
    if(pName!=""){
      this.loader1=false;
      this.service.sendSearch(pName).subscribe(
        data => {console.log("Success !!! ", data)
          // window.alert("Product is Registered and pending for Admin Approval")
          let data1;
          if (data != null && data.length!=0){
            this.allSearcheddata=[];
            for (let i=0;i<data.length;i++){
              data1 = data[i];
              // console.log(data1);
              this.allSearcheddata[i] = new Product();
              this.allSearcheddata[i].productName=data1.productName;
              this.allSearcheddata[i].productPrice=data1.productPrice;
              if(data1.categoryName==null||data1.categoryName==""){
                  this.allSearcheddata[i].category.categoryName="NA";
                  this.allSearcheddata[i].categoryId=null;
                }
              else{
                this.allSearcheddata[i].category.categoryName=data1.categoryName;
                this.allSearcheddata[i].categoryName=data1.categoryName;
                this.allSearcheddata[i].categoryId=data1.categoryId;
              }
              if(data1.productDesc==null){
                this.allSearcheddata[i].productDesc="NA";
                }
              else{
                this.allSearcheddata[i].productDesc=data1.productDesc;
              }
            }
            // console.log(this.allSearcheddata)
          }
          else{
            this.resetSearched();
            // console.log("sdfghjk");
            this.noProd=false;
          }
          this.loader1=true;
        },
        error => {
          this.loader1=true;
          console.log("Error !!!!! ", error)
        }
      ) ;;
      this.toggleDispSearch();
    }
    else{
      // window.alert("Enter a Product Name to search")
      this.msg2="Enter a Product Name to search";
      this.showMsg();
    }

  }

  //update category of product
  updateCategory(){
    this.updatedProduct.categoryName=this.updatedProduct.cat[this.updatedProduct.categoryId];
    console.log(this.updatedProduct);
  }
  //Send Product Data to Server
  addProd(){
    if(this.service.formData.productName!='' && 
        this.service.formData.price!=null && this.service.formData.price>0 &&
        this.service.formData.brand!=null && 
        this.service.formData.category!=null &&
        this.service.formData.color!=null &&
        this.service.formData.productQuantity>0
        ){
      this.loader1=false;
      this.service.formData.createdOn= new Date();
      this.service.formData.productStatus = "pending";
      let arrTemp=[];
      // this.service.formData.imageId = this.imgArray;
      this.service.uploadImg(this.imgArray).subscribe(
        imgurl => {
          this.loader1=true;
          console.log("Success !!! ", imgurl.imgId)
          this.service.formData.imageId = this.service.formData.imageId + ";" + imgurl.imgId;
          this.service.sendProd();
          this.resetForm();
          this.isShow=true;
          this.btnshow=true;
        },
        error => {
          this.loader1=true;
          console.log("Error !????? ", error)
        }
      );
    }
    else{
      this.msg2="Please Enter Data in all Fields";
      this.showMsgBox1("Ok");
    }
  }

  //expand img
  goToLink(url){
    window.open(url);
  }

  //show the products using created by
  showAllProdCreateBy(){
    console.log("Fetching Products of this Vendor");
    this.loader1=false;
    this.service.sendCreatedBy().subscribe(
      data => {
        console.log("Success !!! ", data)
        // window.alert("Product is Registered and pending for Admin Approval")
        let data2;
        if (data != null && data.length!=0){
          this.allCreatedBy=[];
          for (var i=0;i<data.length;i++){
            data2 = data[i];
            // console.log(data2);
            this.allCreatedBy[i] = new Product();
            this.allCreatedBy[i].productStatus=data2.productStatus;
            this.allCreatedBy[i].productId=data2.productId;
            this.allCreatedBy[i].productName=data2.productName;
            this.allCreatedBy[i].productPrice=data2.productPrice;
            this.allCreatedBy[i].image=data2.image;
            this.allCreatedBy[i].brandName=data2.brandName;
            this.allCreatedBy[i].brandId=data2.brandId;
            this.allCreatedBy[i].color=data2.color;
            this.allCreatedBy[i].productQuantity=data2.productQuantity;
            if(data2.categoryName==null||data2.categoryName==""){
                this.allCreatedBy[i].category.categoryName="NA";
                this.allCreatedBy[i].categoryId=null;
                this.allCreatedBy[i].categoryName="NA";
              }
            else{
              this.allCreatedBy[i].category.categoryName=data2.categoryName;
              this.allCreatedBy[i].categoryName=data2.categoryName;
              this.allCreatedBy[i].categoryId=data2.categoryId;
            }
            if(data2.productDesc==null){
              this.allCreatedBy[i].productDesc="NA";
              }
            else{
              this.allCreatedBy[i].productDesc=data2.productDesc;
            }
          }
          // console.log(this.allCreatedBy);
        }
        else{
          this.resetSearchedCreatedBy();
          // console.log("sdfghjk");
          this.noProdCreatedBy=false;
        }
        this.loader1=true;
      },
      error => {
        this.loader1=true;
        console.log("Error !!!!! ", error)

      }
    ) ;;
    this.toggleDispCreatedBy();

  }

  updateProduct(prod:Product){
    console.log(prod);
    this.updatedProduct=prod;
    console.log(this.updatedProduct);
    this.imgArray=[]
    document.getElementById("resultNew").innerHTML=null;
    this.deletedImageIdList=[];
    this.isShow=true;
    this.isSearch=true;
    this.btnshow=true;
    this.noProd=true;
    this.showCreatedBy=true;
    this.noProdCreatedBy=true;
    this.resetForm();
    this.resetSearched();
    this.hideUpdateBox=false;

  }

  cancelUpdateProduct(){
    this.showCreatedBy=false;
    this.hideUpdateBox=true;
  }

  sendUpdateProduct(){
    if(this.updatedProduct.productName!='' && this.updatedProduct.productPrice!=null &&
        this.updatedProduct.productPrice>0 &&
        this.updatedProduct.productQuantity>0){
      this.loader1=false;
      // this.service.formData.createdOn= new Date();
      this.updatedProduct.productStatus = "pending";
      let arrTemp=[];

      // this.service.formData.imageId = this.imgArray;
      this.service.uploadImg(this.imgArray).subscribe(
        imgurl => {
          
          this.loader1=true;
          console.log("Success !!! ", imgurl.imgId)
          // this.updatedProduct.imageIds = this.updatedProduct.imageIds + ";" + imgurl.imgId;
          this.updatedProduct.imageIds = imgurl.imgId;
          this,this.service.deleteImage(this.deletedImageIdList).subscribe(
            data=>{
              this.service.updateOldProd2(this.updatedProduct).subscribe(
                data=>{
                  console.log(data);
                  this.msg2="Updated Successfully";
                  this.showMsg();
                  this.cancelUpdateProduct();
                },
                error=>{
                  this.msg2="Unable to update Product";
                  this.showMsgBox1("Ok");
                  console.log("Error deleting image !????? ", error)
                }
              );
            },
            error=>{

              this.loader1=true;
              this.msg2="Unable to update Product";
              this.showMsgBox1("Ok");
              console.log("Error deleting image !????? ", error)
            }
          )
          //...............................................................change
          // if(val==true){
          //   this.msg2="Updated Successfully";
          //   this.showMsg();
          // }
          // else{
          //   this.msg2="Unable to update Product";
          //   this.showMsgBox1("Ok");
          // }
          this.resetForm();
          this.isShow=true;
          this.btnshow=true;
        },
        error => {
          this.loader1=true;
          this.msg2="Unable to update Product";
          this.showMsgBox1("Ok");
          console.log("Error uploading image !????? ", error)
        }
      );
    }
  }

  ngOnInit() {
    this.checkLogin();
    this.resetForm();
    this.service.getAllCategories().subscribe(
      data=>this.allCategories=data,
      error=>console.log("Error while fetching categories", error)
    )
    this.service.getAllBrands().subscribe(
      dataB=>{
        this.allBrands=dataB;
      },
      error=>console.log("error while fetching all brands", error)
    )
    let sub = this.route.snapshot.paramMap.get('submenu');
    if(sub=="myProducts"){
      this.showAllProdCreateBy();
      this.hideAddProd=true;
    }
    else{
      this.toggleDisplay();
      this.hideAddProd=false;
    }
  }

  resetForm(form? : NgForm){
    if(form!=null)
    {
      form.resetForm();
      form.reset();
    }
    this.service.formData={
    productID:  null,
    productName: '',
    category: '',
    price: null,
    description: '',
    imageId : null,
    productStatus : '',
    createdBy : '',
    createdOn : null,
    modifiedOn : null,
    modifiedBy : '',
    vId: null,
    productQuantity : 1,
    brand:null,
    color:null
    }
    this.imgArray=[];
    this.output = document.getElementById("result");
    this.output.innerHTML=null;
    
    this.output2 = document.getElementById("resultNew");
    this.output2.innerHTML=null;
    this.cnt=0;
    let mainForm = document.getElementById("mainForm");
    

  }
  
  showMsg(){
    this.msgDiag.myfunc();
  }
  showMsgBox1(bName?){
    this.msgDiag.showbox1(bName);
  }
  showMsgBox2(b1Name?,b2Name?){
    this.msgDiag.showbox2(b1Name,b2Name);
  }
  msgResult(val){
    this.msgResultValue=val;
    this.confirmDelete();
  }

  /*
  onSubmit(form:NgForm){
    if(form.value.ProductID==null)
    this.insertRecord(form);

  }
  insertRecord(form:NgForm){
    this.service.postProduct(form.value).subscribe(res=>{
      this.toastr.success('Inserted successfully');
      this.resetForm(form);
    });
  }*/

}

