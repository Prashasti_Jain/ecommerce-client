import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserprofileAdminComponent } from './userprofile-admin.component';

describe('UserprofileAdminComponent', () => {
  let component: UserprofileAdminComponent;
  let fixture: ComponentFixture<UserprofileAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserprofileAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserprofileAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
