import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegistrationService } from '../registration.service';
import { User } from '../user';

@Component({
  selector: 'app-userprofile-admin',
  templateUrl: './userprofile-admin.component.html',
  styleUrls: ['./userprofile-admin.component.css']
})
export class UserprofileAdminComponent implements OnInit {

  // hideSubMenu=true;
  user:User;
  isShow=false;
  // isAddressDetailsShow=true;
  msg = '';
  varfname:string;
  notEditUser=true;

  editUserDetails(){
    this.notEditUser=false;

  }

  noteditUserDetails(){
    this.notEditUser=true;
    this._service.updateUserDetails(this.user).subscribe(
      data=>console.log("User Details updated"),
      error=>console.log("unable to update User Details", error)
    );
    //update userzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
  }

  constructor(private _service:RegistrationService ,private _router:Router) { }

  ngOnInit(): void {
    this.user =new User();
    this.showPersonalDetails();
    // this.fetchAddress();
    //aesdjuyhsadbjgbhsdajhfsadbvjubgfdgsdabbbbbazzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz
  }

  showPersonalDetails(){
    this._service.getUserByEmail().subscribe(
      
      data => {​​
      console.log("response received");
      sessionStorage.setItem("login ","success");
      this.user=<User>data;
      }​​,
      error =>{​​
      console.log("exception occured");
      }​​
    );
  }
 
}
