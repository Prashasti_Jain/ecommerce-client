import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { cartService } from '../shared/cart.service';
import { Product } from '../shared/product';
import { Awsurl } from '../shared/awsurl'
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; 
import { Category } from '../shared/Category';
import { Cartmodel } from '../cartmodel';
import { Image } from '../shared/Image';
import { MsgDialogueComponent } from '../msg-dialogue/msg-dialogue.component';
import { AddproductService } from '../shared/addproduct.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  awsurl = Awsurl.awsUrl;
  products: Product[]=[];
  productsReceived: Array<Product>;
  imgSrc;
  arrimg: string[];
  totalRecords: Number;
  Price: number=0;
  otherCharges: any=0
  totalPrice: any=0;
  quantity: number;
  count: any=0;
  loader1=true;
  initial: number=0;
  cartItems:Cartmodel[]=[];
  prodIds:number[]=[];
  cartCount=0;
  notAvailableItems:Cartmodel[]=[];
  notAvailableProducts:Product[]=[];
  notAvailableProdIds:number[]=[];
  removedProd:Product;
  removedProdNum:number;
  userRole;

  constructor(private router: Router,public service: cartService, public addProdService: AddproductService) { }
  @ViewChild(MsgDialogueComponent) msgDiag:MsgDialogueComponent;
  msg="";
  ngOnInit() {
    this.checkLogin();
    this.checkRole();
    this.service.getAll().subscribe(
      data => this.handleSuccessfulResponse(data),
      error=> console.log("could not fetch cart")
    );
  }
  
  handleSuccessfulResponse(data) {
    console.log("cart items: ",data);
    // this.productsReceived = response;
    
    this.cartCount=0;
    // for(let i=0;i<data.length;i++)
      // this.cartCount+=data[i].quantity;

    for (const item of data) {
      
      let cart1 = new Cartmodel();
      cart1.productId = item.product.productId;
      cart1.cartId = item.cartId;
      cart1.totalPrice = item.totalPrice;
      cart1.quantity = item.quantity;
      if(item.quantity!=0){
        this.Price+=cart1.totalPrice;
        this.otherCharges=0;
        this.totalPrice=parseFloat(this.Price.toString())+parseFloat(this.otherCharges.toString());
        this.count+=cart1.quantity;
        this.cartItems.push(cart1);
        this.prodIds.push(cart1.productId);
      }
      else{
        this.notAvailableItems.push(cart1);
        this.notAvailableProdIds.push(cart1.productId);
      }
    }
    this.service.getAllProducts(this.prodIds).subscribe(
      data=>{
        console.log(data);
        for(let i=0;i<data.length;i++){
          let prod=new Product();
          prod = data[i];
          prod.createdBy = data[i].sellerName;
          if(prod.image.length==0||prod.image==null){
            let img = new Image();
            img.imageURL="../../assets/no-image.jpg";
            prod.image.push(img);
          }
          else{
            for(let i=0;i<prod.image.length;i++){
              prod.image[i].imageURL=this.awsurl+prod.image[i].imageURL;
            }
          }
          this.products.push(prod);
        }
      },
      error=>console.log("Data is unavailable", error)
    )
    if(this.notAvailableProdIds.length!=0){
      this.service.getAllProducts(this.notAvailableProdIds).subscribe(
        data=>{
        console.log(data);
          for(let i=0;i<data.length;i++){
            let prod=new Product();
            prod = data[i];
            prod.createdBy = data[i].sellerName;
            if(prod.image.length==0||prod.image==null){
              let img = new Image();
              img.imageURL="../../assets/no-image.jpg";
              prod.image.push(img);
            }
            else{
              for(let i=0;i<prod.image.length;i++){
                prod.image[i].imageURL=this.awsurl+prod.image[i].imageURL;
              }
            }
            this.notAvailableProducts.push(prod);
          }
        },
        error=>console.log("Data is unavailable", error)
      )
    }
  }
  /*
  plus()
  {
    if(this.quantity!=10)
      this.quantity = this.quantity+1;
  }
  minus()
  {
    if(this.quantity == 1)
      this.quantity = this.quantity;
    else
    this.quantity = this.quantity-1;
  }
  */
  changeQty(prod:Cartmodel, i, change){
      // let price;
      this.quantity = prod.quantity;
      //window.alert(prod.productPrice);
      if(change < 0 && prod.quantity == 1){
        return;
      }
      if(change > 0 && prod.quantity == 10){
        return;
      }
      let id=prod.productId;
      this.quantity = this.quantity + change;
      // price=this.products[i].productPrice;
      prod.quantity = this.quantity;
      //prod.productPrice = quantity * price;
      // price=price*this.quantity;
      if(change==1)
        this.Price+=this.products[i].productPrice;
      else
        this.Price-=this.products[i].productPrice;
      //window.alert(this.Price);
      this.count+=change;
      this.service.changeQty(id,prod).subscribe(
        data => {
          if(data.value=="Added"){
            console.log("Success !!! ", data)
          }
          else{
            // window.alert("Sorry! We don't have any more quantity of this item");
            this.msg="Sorry! We don't have any more quantity of this item";
            this.showMsg();
            this.quantity-=change;
            prod.quantity=this.quantity;
            // price=this.products[i].productPrice*this.quantity;
            this.count-=change;
            if(change==1)
              this.Price-=this.products[i].productPrice;
          }
          //window.alert("Quantity Updated Successfully"); 
        },
        error => {
          console.log("Error !!!!! ", error)
        }
        );
  }

  removeFromCart(prod:Product,i){

    this.removedProd=prod;
    this.removedProdNum=i;
    // console.log(id);
    this.msg="Are you sure you want to remove this product from your cart?";
    this.showMsgBox2("Remove","Cancel");
  }

  confirmRemove(val){
    let prod=this.removedProd;
    let i = this.removedProdNum;
    let id=prod.productId;
    if (val==1) {
      
      console.log('Product is getting deleted from the cart.');
      this.quantity = this.cartItems[i].quantity;
      this.Price = this.Price - (prod.productPrice * this.cartItems[i].quantity);
      this.service.removeFromCart(id).subscribe(
        data => {console.log("Success !!! ", data)
          // window.alert("Product Removed Successfully") 
          this.msg="Product Removed Successfully";
          this.showMsg();
          console.log(this.products);
          const index: number = this.products.indexOf(prod);
          if (index != -1) {
              this.products.splice(index, 1);
              this.prodIds.splice(index, 1);
              this.count-=this.cartItems[i].quantity;
              this.cartItems.splice(i,1);
              console.log(this.products);
          }
        },
        error => {
          console.log("Error !!!!! ", error)
          // window.alert("Please Refresh the page");
          this.msg="Please Refresh the page";
          this.showMsg();
        }
      ) ;
    } 
    else {
      console.log('Product is not deleted from the database.');
    }
  }

  removeFromNACart(prod:Product,i){
    let id=prod.productId;
    console.log(id);
      console.log('Product is getting deleted from the cart.');
      this.service.removeFromCart(id).subscribe(
        data => {console.log("Success !!! ", data)
          // window.alert("Product Removed Successfully") 
          this.msg="Product Removed Successfully";
          this.showMsg();
          console.log(this.notAvailableProducts);
          const index: number = this.notAvailableProducts.indexOf(prod);
          if (index != -1) {
              this.notAvailableProducts.splice(index, 1);
              this.notAvailableProdIds.splice(index, 1);
              this.notAvailableItems.splice(i,1);
              console.log(this.notAvailableProducts);
          }
        },
        error => {
          console.log("Error !!!!! ", error)
          // window.alert("Please Refresh the page");
          this.msg="Please Refresh the page";
          this.showMsg();
        }
      ) ;
  }



  saveForLater(productSave,i){
    console.log("adding to wishlist now");
    let productId = productSave.productId;
    this.addProdService.addToWishlist(productId).subscribe(
      data=>{
        console.log("added");
        // this.msg="Item Saved for Later";
        // this.showMsg();
        this.removeFromCart(productSave,i);
      },
      error=>console.log(error)
    )
  }
  placeOrder(){
    if(this.notAvailableProducts.length==0){
      //call order function to upload in server
      console.log("sdffhsdfh");
      this.router.navigate(['/placeorder']);
    }
    else
    {
      // window.alert("Please remove all unavailable products from cart")
      this.msg="Please remove all unavailable products from cart";
      this.showMsg();
    }
  }
  goToProduct(prod){
    this.router.navigate(['/product', prod.productId]);
  }


  checkRole(){
    this.userRole=window.localStorage.getItem("userRole");
  }




  showMsg(){
    this.msgDiag.myfunc();
  }
  showMsgBox1(bName?){
    this.msgDiag.showbox1(bName);
  }
  showMsgBox2(b1Name?,b2Name?){
    this.msgDiag.showbox2(b1Name,b2Name);
  }
  msgResult(val){
    this.confirmRemove(val);
    console.log(val);
  }
  checkLogin(){
    let loginUserRole=window.localStorage.getItem("userRole");
    console.log(loginUserRole);
    if(loginUserRole==null)
    {
      console.log("Please login");
      this.router.navigate(["/login"]);
    }
  }
  

}
