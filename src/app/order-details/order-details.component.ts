import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Address } from '../address';
import { MsgDialogueComponent } from '../msg-dialogue/msg-dialogue.component';
import { Order } from '../order';
import { RegistrationService } from '../registration.service';
import { AddproductService } from '../shared/addproduct.service';
import { Awsurl } from '../shared/awsurl';
import { cartService } from '../shared/cart.service';
import { Image } from '../shared/Image';
import { Product } from '../shared/product';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {
  @ViewChild(MsgDialogueComponent) msgDiag:MsgDialogueComponent;
  msg="";
  userRole;
  cartCount=0;
  loader1=false;
  orders:Order[]=[];
  products:Product[]=[];
  prodIds=[];
  allAddresses: Address[]=[];
  addrList:number[]=[];
  awsUrl = Awsurl.awsUrl;
  hideOrders=true;
  page=1;
  hideOrderDet:boolean[]=[];
  rateProdId:number;
  rateProdName:string;
  rateProdRating:number;
  rateProdReview:string;
  rateProdHeading:string;
  showRatingDiv=false;
  constructor(public serviceProd: AddproductService, public serviceCart: cartService,public serviceReg: RegistrationService, private router: Router) { }


  checkLogin(){
    let loginUserRole=window.localStorage.getItem("userRole");
    console.log(loginUserRole);
    if(loginUserRole==null)
    {
      console.log("Please login");
      this.router.navigate(["/login"]);
    }
  }
  

  hideAllAddrs(){
    for(let i=0;i<this.hideOrderDet.length;i++){
      this.hideOrderDet[i]=true;
    }
  }

  showAddr(i:number){
    console.log(i);
    this.hideAllAddrs();
    this.hideOrderDet[i]=false;
  }

  getAllOrders(){
    this.serviceProd.getMyOrders().subscribe(
      data=>{
        this.page=1;
        this.orders=data;
        console.log(data);
        this.prodIds=[];
        this.addrList=[];
        this.hideOrderDet=[];
        // let prodIdSet = new Set();
        for(let i=0;i<this.orders.length;i++){
          // prodIdSet.add(this.orders[i].productId);
          this.prodIds.push(this.orders[i].productId);
          this.addrList.push(this.orders[i].addressId);
          this.hideOrderDet.push(true);
          if(this.orders[i].date==null){
            this.orders[i].date="NA";
          }
        }
        // this.prodIds = Array.from(prodIdSet);
        this.serviceCart.getAllProducts(this.prodIds).subscribe(
          data2=>{
            this.products=data2;
            for(let j=0;j<this.products.length;j++){
              this.products[j].createdBy=data2[j].sellerName;
              if(this.products[j].image.length==0 || this.products[j].image==null){
                let img = new Image();
                img.imageId=null;
                img.imageURL="../../assets/no-image.jpg";
                this.products[j].image.push(img);
              }
              else{
                this.products[j].image[0].imageURL=this.awsUrl+this.products[j].image[0].imageURL;
              }
            }
            console.log(data2);
            this.serviceReg.getMyOrderAddress(this.addrList).subscribe(
              data3=>{
                console.log(data3);
                this.allAddresses=data3;
                
                this.loader1=true;
                this.hideOrders=false;
              },
              error=>console.log("error while fetching address",error)
            )
          },
          error=> console.log("could not fetch product",error)
        )
      },
      error=>console.log("error while fetching order",error)
    )
  }

  checkRole(){
    this.userRole=window.localStorage.getItem("userRole");
  }
  showMsg(){
    this.msgDiag.myfunc();
  }


  ngOnInit(): void {
    this.checkLogin();
    this.checkRole();
    
    this.loader1=false;
    this.hideOrders=true;
    this.getAllOrders();
  }

  rateProduct(id,name){
    this.loader1=false;
    this.rateProdId=id;
    this.rateProdName=name;
    this.serviceProd.getMyRating(id).subscribe(
      ratingData=>{
        console.log(ratingData);
        this.rateProdRating=ratingData.rating;
        this.rateProdReview=ratingData.comment;
        this.rateProdHeading=ratingData.heading;
        this.loader1=true;
        this.showRatingDiv=true;
      },
      error=>{
        this.rateProdRating=null;
        this.rateProdReview=null;
        this.rateProdHeading=null;
        this.loader1=true;
        this.showRatingDiv=true;
      }
    )

    // this.rateProdId=null;
  }
  ratingComplete(val){
    this.showRatingDiv=false;
    this.rateProdId=null;
    this.rateProdName=null;
    this.rateProdRating=null;
    this.rateProdReview=null;
    this.rateProdHeading=null;
    console.log("rating value=",val);
  }
}
