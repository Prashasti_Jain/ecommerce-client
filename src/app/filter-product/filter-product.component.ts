import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Product } from '../shared/product';

@Component({
  selector: 'app-filter-product',
  templateUrl: './filter-product.component.html',
  styleUrls: ['./filter-product.component.css']
})
export class FilterProductComponent implements OnInit {

  // @Input() allProds:Product[];
  allProds:Product[]=[];
  @Output() result = new EventEmitter<any>();

  filteredProds:Product[]=[]
  colors;
  // colorValues;
  brands;
  // brandValues;
  categories;
  // categoryValues;
  priceMax:number=0;
  minPrice:number=0;
  pricePerc;
  maxPrice:number;

  hideCatg=false;
  hideColor=false;
  hideBrand=false;
  hideAvailability=false;
  availabilityCheck=false;

  constructor() { }

  ngOnInit(): void {
  }

  populate(allProds){
    this.allProds=allProds;
    console.log("dzxgfsdzxg", this.allProds);
    this.categories=[];
    // this.categoryValues=[];
    this.colors=[];
    // this.colorValues=[];
    this.brands=[];
    // this.brandValues=[];

    this.filteredProds=[];
    let catgSet = new Set();
    let catgArray=[]
    let colorSet = new Set();
    let colorArray=[]
    let brandSet = new Set();
    let brandArray=[]
    if(this.allProds!=null){
      this.allProds.forEach(x=>{
        if(x.categoryName!=null)
          catgSet.add(x.categoryName);
        if(x.color!=null)
          colorSet.add(x.color);
        if(x.brandName!=null)
          brandSet.add(x.brandName);
        this.filteredProds.push(x);

        if(x.productPrice>this.priceMax){
          this.priceMax=x.productPrice;
        }
      })
      
      catgArray=Array.from(catgSet);
      colorArray=Array.from(colorSet);      
      brandArray=Array.from(brandSet)
      
      catgArray.forEach(y=>this.categories.push({"name":y,"value":false}))
      colorArray.forEach(y=>this.colors.push({"name":y,"value":false}))
      brandArray.forEach(y=>this.brands.push({"name":y,"value":false}))
    }
    this.maxPrice=this.priceMax;
    // this.categories=[{"name":"abc","value":false},{"name":"degfhj","value":false},{"name":"rtyugrfdg","value":false},{"name":"gfcv","value":false}];
    
    // this.colors=[{"name":"abc","value":false},{"name":"degfhj","value":false},{"name":"rtyugrfdg","value":false},{"name":"gfcv","value":false}];
    
    // this.brands=[{"name":"abc","value":false},{"name":"degfhj","value":false},{"name":"rtyugrfdg","value":false},{"name":"gfcv","value":false}];
  
  }



  filterProd(){
    console.log("filtering prods")
    this.filteredProds=this.allProds;
    let newProds:Product[];

    //category filter
    newProds=[];
    let x_cat=0;
    for(let i=0;i<this.categories.length;i++){
      if(this.categories[i].value){
        x_cat=1;
        break;
      }
    }
    if(x_cat==1){
      this.filteredProds.forEach(x=>{
        for(let j=0;j<this.categories.length;j++){
          if(x.categoryName==this.categories[j].name && this.categories[j].value==true){
            newProds.push(x);
          }
        }
      })
      this.filteredProds=[];
      newProds.forEach(y=>this.filteredProds.push(y));
    }

    //color filter
    newProds=[];
    let x_col=0;
    for(let i=0;i<this.colors.length;i++){
      if(this.colors[i].value){
        x_col=1;
        break;
      }
    }
    if(x_col==1){
      this.filteredProds.forEach(x=>{
        for(let j=0;j<this.colors.length;j++){
          if(x.color==this.colors[j].name && this.colors[j].value==true){
            newProds.push(x);
          }
        }
      })
      this.filteredProds=[];
      newProds.forEach(y=>this.filteredProds.push(y));
    }

    //brand filter
    newProds=[];
    let x_brand=0;
    for(let i=0;i<this.brands.length;i++){
      if(this.brands[i].value){
        x_brand=1;
        break;
      }
    }
    if(x_brand==1){
      this.filteredProds.forEach(x=>{
        for(let j=0;j<this.brands.length;j++){
          if(x.brandName==this.brands[j].name && this.brands[j].value==true){
            newProds.push(x);
          }
        }
      })
      this.filteredProds=[];
      newProds.forEach(y=>this.filteredProds.push(y));
    }


    //price filter
    newProds=[];
    this.filteredProds.forEach(x=>{
      if(x.productPrice>=this.minPrice && x.productPrice<=this.maxPrice){
        newProds.push(x);
      }
    })
    this.filteredProds=[];
    newProds.forEach(y=>this.filteredProds.push(y));

    //return the products
    // console.log(this.filteredProds);
    this.returnFilterdProds();



    //availability filter
    newProds=[];
    if(this.availabilityCheck){
      this.filteredProds.forEach(x=>{
        if(x.productQuantity>0){
          newProds.push(x);
        }
      })
      this.filteredProds=[];
      newProds.forEach(y=>this.filteredProds.push(y));
    }


    //return the products
    // console.log(this.filteredProds);
    this.returnFilterdProds();
  }

  clearAll(){
    for(let i=0;i<this.categories.length;i++)
      this.categories[i].value=false;
    for(let i=0;i<this.colors.length;i++)
      this.colors[i].value=false;
    for(let i=0;i<this.brands.length;i++)
      this.brands[i].value=false;

    this.maxPrice=this.priceMax;
    this.availabilityCheck=false;
    
    this.filterProd();
  }

  //return the productsto parent component
  returnFilterdProds(){
    this.result.emit(this.filteredProds)
  }

}
